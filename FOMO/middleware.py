from catalog import models

def HistoryMiddleware(get_response):

    def middleware(request):
        print('>>>>>> middleware beginning')

        last5_list = request.session.get('last5', [])
        request.last5 = last5_list
        print('>>>>>> ', request.last5)
        lastfive = request.last5

        response = get_response(request)

        request.session['last5'] = request.last5[:6]
        return response

        print('>>>>>> middleware ending')

    return middleware

# -*- coding: utf-8 -*-
# Generated by Django 1.10.5 on 2017-02-26 00:28
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('account', '0002_auto_20170223_2052'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='fomouser',
            options={'verbose_name': 'user', 'verbose_name_plural': 'users'},
        ),
    ]

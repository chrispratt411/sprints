# -*- coding: utf-8 -*-
# Generated by Django 1.10.5 on 2017-03-31 19:18
from __future__ import unicode_literals

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('catalog', '0004_product_description'),
        ('account', '0009_auto_20170331_1248'),
    ]

    operations = [
        migrations.CreateModel(
            name='LastFive',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('viewDate', models.DateTimeField(blank=True, null=True)),
                ('productId', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='catalog.Product')),
                ('userId', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.RemoveField(
            model_name='userproduct',
            name='cartorview',
        ),
        migrations.AlterField(
            model_name='userproduct',
            name='productId',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='catalog.Product'),
        ),
    ]

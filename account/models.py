from django.db import models
from django.db import models
from django.contrib.auth.models import AbstractUser
from datetime import datetime
from django import forms
from catalog import models as cmod
from decimal import Decimal

# Create your models here.
class FomoUser(AbstractUser):
    # inheriting from super - id, first_name, last_name, email
    street_address = models.TextField(null=True, blank=True)
    city = models.TextField(null=True, blank=True)
    state = models.TextField(null=True, blank=True)
    zip_code = models.TextField(null=True, blank=True)
    phone = models.TextField(null=True, blank=True)
    birthdate = models.DateField(null=True, blank=True)

    def get_cart_count(self):
        return ShoppingCartItem.objects.filter(user = self).count()

    def calc_Shipping(self):
        return Decimal(10.00)

    def shoppingCart(self):
        return ShoppingCartItem.objects.filter(user = self)

    def calc_subTotal(self):
        subTotal = 0
        for product in self.shoppingCart():
            subTotal = subTotal + (product.product.price * product.quantity)
        return subTotal

    def clear_cart(self):
        self.shoppingCart().delete()

    # def finalize_sale(self):
        # for item in cart:
        #     item.purchased = True
        #     product = cmod.Product.objects.get(id=item.product.id)
        #     if product.quantity == 0:
        #         product.available = False

    def calc_Tax(self):
        return Decimal("{0:.2f}".format(self.calc_subTotal() * Decimal(.0725)))

    def calc_Total(self):
        return self.calc_Shipping() + self.calc_subTotal() + self.calc_Tax()

class ShoppingCartItem(models.Model):
    user = models.ForeignKey('FomoUser', null=True)
    product = models.ForeignKey('catalog.Product', null=True)
    purchased = models.BooleanField(default=False)
    quantity = models.IntegerField()

class History(models.Model):
    user = models.ForeignKey('FomoUser', null=True)
    product = models.ForeignKey('catalog.Product')
    viewDate = models.DateTimeField(null=True, blank=True)
    addedToCart = models.BooleanField(default=False)
    purchased = models.BooleanField(default=False)

class SaleRecord(models.Model):
    user = models.ForeignKey('FomoUser', null=True)
    # shipping = models.ForeignKey('FomoUser.shipping')
    full_name = models.TextField(null=True, blank=True)
    street_address = models.TextField(null=True, blank=True)
    city = models.TextField(null=True, blank=True)
    state = models.TextField(null=True, blank=True)
    postal_code = models.TextField(null=True, blank=True)
    subtotal = models.DecimalField(decimal_places=2, max_digits=7, null=True, blank=True)
    stripeID = models.TextField()
    tax = models.DecimalField(decimal_places=2, max_digits=7, null=True, blank=True)
    total = models.DecimalField(null=True, blank=True, decimal_places=2, max_digits=7)

class SaleItem(models.Model):
    category_choices = (('Product', 'Product'), ('Fee', 'Fee'))

    product = models.ForeignKey('catalog.Product', null=True, blank=True)
    category = models.TextField(choices=category_choices, default='Product')
    price = models.DecimalField(null=True, blank=True, decimal_places=2, max_digits=7)
    sale = models.ForeignKey('SaleRecord')

class Payment(models.Model):
    sale = models.ForeignKey('SaleRecord')
    amount = models.DecimalField(decimal_places=2, max_digits=7)
    stripe_ID = models.TextField()

$(function() {
  $("#id_birthdate").datetimepicker({
    timepicker:false,
    format:'Y-m-d'
  });

  $('#delete-user').click(function(event) {
    // don't do the normal behavior
    event.preventDefault();

    var link = $(this).attr('href');
    console.log(link);

    $('#delete-button').attr('href', link);

    $('#myModal').modal();
  });

});

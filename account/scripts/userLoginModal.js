$(function() {
  $('#form-container > form').ajaxForm({
    target: '#jquery-loadmodal-js-body',
  });

  var images = $('#jquery-loadmodal-js-body img');
  var image = 0;

  function showPic() {
    images.hide();
    var current = $(images[image]);
    current.show();
  }

  showPic(image);

  $('#next_pic_button').click(function() {
    image = image + 1;
    if (image == images.length)
      image = 0;
    showPic(image);
  });

  $('#previous_pic_button').click(function() {
    image = image - 1;
    if (image < 0)
      image = images.length - 1;
    showPic(image);
  });


});

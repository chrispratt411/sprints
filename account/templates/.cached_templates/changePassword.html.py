# -*- coding:utf-8 -*-
from mako import runtime, filters, cache
UNDEFINED = runtime.UNDEFINED
STOP_RENDERING = runtime.STOP_RENDERING
__M_dict_builtin = dict
__M_locals_builtin = locals
_magic_number = 10
_modified_time = 1488655468.5169473
_enable_loop = True
_template_filename = 'C:/Users/coolp/Google Drive/IS 411 - Systems Design and Implementation/FOMO/account/templates/changePassword.html'
_template_uri = 'changePassword.html'
_source_encoding = 'utf-8'
import os, os.path, re, json
import django_mako_plus
_exports = ['content', 'body_above']


def _mako_get_namespace(context, name):
    try:
        return context.namespaces[(__name__, name)]
    except KeyError:
        _mako_generate_namespaces(context)
        return context.namespaces[(__name__, name)]
def _mako_generate_namespaces(context):
    pass
def _mako_inherit(template, context):
    _mako_generate_namespaces(context)
    return runtime._inherit_from(context, 'app_base.htm', _template_uri)
def render_body(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        __M_locals = __M_dict_builtin(pageargs=pageargs)
        form = context.get('form', UNDEFINED)
        def content():
            return render_content(context._locals(__M_locals))
        def body_above():
            return render_body_above(context._locals(__M_locals))
        account = context.get('account', UNDEFINED)
        __M_writer = context.writer()
        __M_writer('\r\n\r\n')
        if 'parent' not in context._data or not hasattr(context._data['parent'], 'body_above'):
            context['self'].body_above(**pageargs)
        

        __M_writer('\r\n\r\n')
        if 'parent' not in context._data or not hasattr(context._data['parent'], 'content'):
            context['self'].content(**pageargs)
        

        __M_writer('\r\n')
        return ''
    finally:
        context.caller_stack._pop_frame()


def render_content(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        form = context.get('form', UNDEFINED)
        def content():
            return render_content(context)
        account = context.get('account', UNDEFINED)
        __M_writer = context.writer()
        __M_writer('\r\n    <h1>Change Password</h1>\r\n    <h2>')
        __M_writer(str( account.first_name ))
        __M_writer(' ')
        __M_writer(str( account.last_name ))
        __M_writer('</h2>\r\n    ')
        __M_writer(str( form ))
        __M_writer('\r\n\r\n')
        return ''
    finally:
        context.caller_stack._pop_frame()


def render_body_above(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        def body_above():
            return render_body_above(context)
        __M_writer = context.writer()
        __M_writer('\r\n  <ol class="breadcrumb">\r\n    <li class="breadcrumb-item"><a href="/homepage/">Homepage</a></li>\r\n    <li class="breadcrumb-item"><a href="/account/users/">Users</a></li>\r\n    <li class="breadcrumb-item active">Change Password</li>\r\n  </ol>\r\n')
        return ''
    finally:
        context.caller_stack._pop_frame()


"""
__M_BEGIN_METADATA
{"uri": "changePassword.html", "source_encoding": "utf-8", "filename": "C:/Users/coolp/Google Drive/IS 411 - Systems Design and Implementation/FOMO/account/templates/changePassword.html", "line_map": {"64": 11, "65": 13, "66": 13, "67": 13, "68": 13, "69": 14, "70": 14, "40": 1, "76": 3, "45": 9, "88": 82, "50": 16, "82": 3, "56": 11, "29": 0}}
__M_END_METADATA
"""

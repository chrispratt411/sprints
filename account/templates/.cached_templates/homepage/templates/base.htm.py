# -*- coding:utf-8 -*-
from mako import runtime, filters, cache
UNDEFINED = runtime.UNDEFINED
STOP_RENDERING = runtime.STOP_RENDERING
__M_dict_builtin = dict
__M_locals_builtin = locals
_magic_number = 10
_modified_time = 1491692974.144484
_enable_loop = True
_template_filename = 'C:/Users/coolp/Google Drive/IS 411 - Systems Design and Implementation/FOMO/homepage/templates/base.htm'
_template_uri = '/homepage/templates/base.htm'
_source_encoding = 'utf-8'
import os, os.path, re, json
import django_mako_plus
_exports = ['header', 'body_right', 'maintenance_message', 'body_left', 'body_above', 'header_items', 'footer', 'title', 'content']


from django_mako_plus import get_template_css, get_template_js 

from datetime import datetime 

def render_body(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        __M_locals = __M_dict_builtin(pageargs=pageargs)
        def header():
            return render_header(context._locals(__M_locals))
        def body_right():
            return render_body_right(context._locals(__M_locals))
        user = context.get('user', UNDEFINED)
        def maintenance_message():
            return render_maintenance_message(context._locals(__M_locals))
        request = context.get('request', UNDEFINED)
        def footer():
            return render_footer(context._locals(__M_locals))
        def header_items():
            return render_header_items(context._locals(__M_locals))
        def title():
            return render_title(context._locals(__M_locals))
        self = context.get('self', UNDEFINED)
        STATIC_URL = context.get('STATIC_URL', UNDEFINED)
        def content():
            return render_content(context._locals(__M_locals))
        def body_left():
            return render_body_left(context._locals(__M_locals))
        def body_above():
            return render_body_above(context._locals(__M_locals))
        realHistory = context.get('realHistory', UNDEFINED)
        __M_writer = context.writer()
        __M_writer('\n')
        __M_writer('\n\n\n<!DOCTYPE html>\n<html>\n    <meta charset="UTF-8">\n    <head>\n\n        <link rel="icon" alt="icon" href="')
        __M_writer(str(STATIC_URL))
        __M_writer('homepage/media/Images/music_note.png" />\n        <title>\n          ')
        if 'parent' not in context._data or not hasattr(context._data['parent'], 'title'):
            context['self'].title(**pageargs)
        

        __M_writer('\n        </title>\n\n')
        __M_writer('        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>\n        <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.5/js/bootstrap.min.js"></script>\n        <script src="')
        __M_writer(str( STATIC_URL ))
        __M_writer('account/scripts/jquery.datetimepicker.full.js"></script>\n        <script src="')
        __M_writer(str( STATIC_URL ))
        __M_writer('homepage/media/jquery.loadmodal.js"></script>\n        <script src="')
        __M_writer(str( STATIC_URL ))
        __M_writer('homepage/media/jquery.form.js"></script>\n\n        <link rel="stylesheet" href="')
        __M_writer(str(STATIC_URL))
        __M_writer('account/media/jquery.datetimepicker.css" />\n        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Josefin+Slab" />\n        <link rel="stylesheet" href="')
        __M_writer(str(STATIC_URL))
        __M_writer('homepage/media/bootstrap/css/bootstrap.css" />\n        <!-- <link href="https://fonts.googleapis.com/css?family=Josefin+Sans" rel="stylesheet"> -->\n')
        if request.dmp_router_page == 'sections':
            __M_writer('        <link rel="stylesheet" href="')
            __M_writer(str(STATIC_URL))
            __M_writer('homepage/media/sections.css" />\n')
        if request.dmp_router_page == 'about':
            __M_writer('        <link rel="stylesheet" href="')
            __M_writer(str(STATIC_URL))
            __M_writer('homepage/media/about.css" />\n')
        if request.dmp_router_page == 'terms':
            __M_writer('        <link rel="stylesheet" href="')
            __M_writer(str(STATIC_URL))
            __M_writer('homepage/media/terms.css" />\n')
        if request.dmp_router_page == 'contact':
            __M_writer('        <link rel="stylesheet" href="')
            __M_writer(str(STATIC_URL))
            __M_writer('homepage/media/contact.css" />\n')
        if request.dmp_router_page == 'faq':
            __M_writer('        <link rel="stylesheet" href="')
            __M_writer(str(STATIC_URL))
            __M_writer('homepage/media/faq.css" />\n')
        __M_writer('        ')
        __M_writer(str( get_template_css(self, request, context) ))
        __M_writer('\n\n    </head>\n\n    <div id="maintenance" style="text-align: center; color: white;">\n      ')
        if 'parent' not in context._data or not hasattr(context._data['parent'], 'maintenance_message'):
            context['self'].maintenance_message(**pageargs)
        

        __M_writer('\n    </div>\n\n    <header>\n      <nav id="my-nav" class="navbar navbar-default">\n        <div class="container-fluid">\n            <div class="navbar-header">\n              <a class="navbar-brand" style="padding-top: 0px; padding-left:10px; padding-right:0px;" href="/homepage/"><img src="')
        __M_writer(str(STATIC_URL))
        __M_writer('homepage/media/Images/music note icon.png" alt="music note" style="height: 67px" /></a>\n            </div>\n          ')
        if 'parent' not in context._data or not hasattr(context._data['parent'], 'header_items'):
            context['self'].header_items(**pageargs)
        

        __M_writer('\n          <ul class="nav navbar-nav navbar-right">\n')
        if user.is_authenticated:
            __M_writer('            <li class="dropdown">\n              <a class="dropdown-toggle" style="margin-right:15px;" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="true">Hello, ')
            __M_writer(str(user.username))
            __M_writer('<span class="caret"></span></a>\n              <ul class="dropdown-menu" style = "margin-right: 15px;">\n                <li><a href="/account/userLogout">Logout</a></li>\n                <li><a href="/account/users/">User Manager</a></li>\n                <li><a href="/manager/">Instrument Manager</a></li>\n                <li><a href="/account/manage/')
            __M_writer(str(user.username))
            __M_writer('">My Account</a></li>\n                <li><a href="/account/changePassword/')
            __M_writer(str(user.username))
            __M_writer('">Change Password</a></li>\n              </ul>\n            </li>\n')
            if user.is_authenticated:
                __M_writer('            <li><a style="padding-top:10px; height: 67px;" class="navbar-brand" href="/account/shoppingCart"><img src="')
                __M_writer(str(STATIC_URL))
                __M_writer('homepage/media/Images/cart.png" alt="cart" style="padding-right: 10px; height: 47px" /><div id="cart-count" class="label label-danger" style="color: black; background-color: #5bc0de; height: 18px; padding: 3px; width: 18px; font-size: 14px; position:absolute; top:4px; left:50px;">')
                __M_writer(str( request.user.get_cart_count() ))
                __M_writer('</div></a>\n')
        else:
            __M_writer('            <li>\n              <a href="/account/userLogin"><span class="glyphicon glyphicon-log-in"></span> Login</a>\n            </li>\n')
        __M_writer('          </ul>\n        </div>\n      </nav>\n\n')
        if user.is_authenticated and request.dmp_router_app == 'homepage':
            __M_writer('      <div class="alert_message alert alert-info">\n        <a href="#" class="close" data-dismiss="alert" aria-label="close" style="color: white;">&times;</a>\n          <strong>You logged on successfully!&nbsp&nbsp</strong>\n      </div>\n')
        elif request.dmp_router_page == 'sections':
            __M_writer('      <div class="alert_message alert alert-info">\n        <a href="#" class="close" data-dismiss="alert" aria-label="close" style="color: white;">&times;</a>\n          <strong>You logged on successfully!&nbsp&nbsp</strong>\n      </div>\n')
        __M_writer('\n    </header>\n    <body>\n      <div class="body_above">\n        <div id="F" style="color: #df691a">F</div>OMO<div id="F" style="color: #df691a">F</div>OMO<div id="F" style="color: #df691a">F</div>OMO<div id="F" style="color: #df691a">F</div>OMO<div id="F" style="color: #df691a">F</div>\n        OMO<div id="F" style="color: #df691a">F</div>OMO<div id="F" style="color: #df691a">F</div>OMO<div id="F" style="color: #df691a">F</div>OMO<div id="F" style="color: #df691a">F</div>OMO<div id="F" style="color: #df691a">F</div>OMO<div id="F" style="color: #df691a">F</div>\n        OMO<div id="F" style="color: #df691a">F</div>OMO<div id="F" style="color: #df691a">F</div>OMO<div id="F" style="color: #df691a">F</div>OMO<div id="F" style="color: #df691a">F</div>OMO<div id="F" style="color: #df691a">F</div>OMO<div id="F" style="color: #df691a">F</div>\n        OMO<div id="F" style="color: #df691a">F</div>OMO<div id="F" style="color: #df691a">F</div>OMO<div id="F" style="color: #df691a">F</div>OMO<div id="F" style="color: #df691a">F</div>OMO<div id="F" style="color: #df691a">F</div>OMO<div id="F" style="color: #df691a">F</div>\n        OMO<div id="F" style="color: #df691a">F</div>OMO<div id="F" style="color: #df691a">F</div>OMO<div id="F" style="color: #df691a">F</div>OMO<div id="F" style="color: #df691a">F</div>OMO<div id="F" style="color: #df691a">F</div>OMO<div id="F" style="color: #df691a">F</div>\n\n        ')
        if 'parent' not in context._data or not hasattr(context._data['parent'], 'body_above'):
            context['self'].body_above(**pageargs)
        

        __M_writer('\n      </div>\n\n      <div class="body_left" alt="violin">\n        ')
        if 'parent' not in context._data or not hasattr(context._data['parent'], 'body_left'):
            context['self'].body_left(**pageargs)
        

        __M_writer('\n      </div>\n\n      <div class="body_middle">\n        <header>\n          <h1 style="color: #5bc0de">')
        if 'parent' not in context._data or not hasattr(context._data['parent'], 'header'):
            context['self'].header(**pageargs)
        

        __M_writer('</h1>\n        </header>\n\n        <div id="content">\n          ')
        if 'parent' not in context._data or not hasattr(context._data['parent'], 'content'):
            context['self'].content(**pageargs)
        

        __M_writer('\n        </div>\n        <!-- ')
        if 'parent' not in context._data or not hasattr(context._data['parent'], 'footer'):
            context['self'].footer(**pageargs)
        

        __M_writer(' -->\n      </div>\n\n      <div class="body_right" alt="guitar">\n        ')
        if 'parent' not in context._data or not hasattr(context._data['parent'], 'body_right'):
            context['self'].body_right(**pageargs)
        

        __M_writer('\n      </div>\n\n\n\n')
        __M_writer('      ')
        __M_writer(str( get_template_js(self, request, context) ))
        __M_writer('\n    </body>\n\n    <script type="text/javascript">\n      $(document).ready(function(){\n          $(".alert_message").hide().fadeIn(800).delay(3000).fadeOut(800);\n      });\n    </script>\n\n</html>\n')
        return ''
    finally:
        context.caller_stack._pop_frame()


def render_header(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        def header():
            return render_header(context)
        __M_writer = context.writer()
        return ''
    finally:
        context.caller_stack._pop_frame()


def render_body_right(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        def body_right():
            return render_body_right(context)
        STATIC_URL = context.get('STATIC_URL', UNDEFINED)
        user = context.get('user', UNDEFINED)
        realHistory = context.get('realHistory', UNDEFINED)
        __M_writer = context.writer()
        __M_writer('\n')
        if user.is_authenticated():
            __M_writer('        <h1 style="color: #5bc0de;">Recently viewed.</h1>\n        <ul class="nav">\n')
            for p in realHistory:
                __M_writer('          <li><a style="color: #df691a; font-size: 30px;" href="/catalog/product/')
                __M_writer(str( p.id ))
                __M_writer('">')
                __M_writer(str( p.name ))
                __M_writer('<img style="width: 60px; padding-left: 10px" src="')
                __M_writer(str( STATIC_URL ))
                __M_writer('homepage/media/Images/')
                __M_writer(str( p.name ))
                __M_writer('.svg" /></a></li>\n')
            __M_writer('        </ul>\n')
        __M_writer('        ')
        return ''
    finally:
        context.caller_stack._pop_frame()


def render_maintenance_message(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        def maintenance_message():
            return render_maintenance_message(context)
        __M_writer = context.writer()
        __M_writer('This site will be down for scheduled maintenance on Saturday, February 11 from 12:00AM - 3:00AM ET.')
        return ''
    finally:
        context.caller_stack._pop_frame()


def render_body_left(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        def body_left():
            return render_body_left(context)
        __M_writer = context.writer()
        return ''
    finally:
        context.caller_stack._pop_frame()


def render_body_above(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        def body_above():
            return render_body_above(context)
        __M_writer = context.writer()
        __M_writer('\n\n        <nav class="breadcrumb">\n          <span class="breadcrumb-item active">Homepage</span>\n        </nav>\n\n        ')
        return ''
    finally:
        context.caller_stack._pop_frame()


def render_header_items(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        def header_items():
            return render_header_items(context)
        __M_writer = context.writer()
        __M_writer('\n          ')
        return ''
    finally:
        context.caller_stack._pop_frame()


def render_footer(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        def footer():
            return render_footer(context)
        __M_writer = context.writer()
        __M_writer('\n          <footer style="padding-top: 15px;">\n            <div class="footer">\n              ')
        __M_writer('\n              ')
        now = datetime.now() 
        
        __M_writer('\n              ')
        year = now.year 
        
        __M_writer('\n              -- &copy ')
        __M_writer(str(year))
        __M_writer(' Copyright -- Family Oriented Music Organization -- All rights reserved --\n            </div>\n          </footer>\n        ')
        return ''
    finally:
        context.caller_stack._pop_frame()


def render_title(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        def title():
            return render_title(context)
        __M_writer = context.writer()
        __M_writer('Homepage - FOMO')
        return ''
    finally:
        context.caller_stack._pop_frame()


def render_content(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        def content():
            return render_content(context)
        __M_writer = context.writer()
        __M_writer('\n\n          ')
        return ''
    finally:
        context.caller_stack._pop_frame()


"""
__M_BEGIN_METADATA
{"line_map": {"258": 128, "259": 131, "260": 132, "262": 132, "263": 133, "265": 133, "266": 134, "267": 134, "273": 14, "18": 4, "20": 131, "22": 0, "279": 14, "285": 124, "291": 124, "297": 291, "50": 2, "51": 4, "52": 12, "53": 12, "58": 14, "59": 18, "60": 20, "61": 20, "62": 21, "63": 21, "64": 22, "65": 22, "66": 24, "67": 24, "68": 26, "69": 26, "70": 28, "71": 29, "72": 29, "73": 29, "74": 31, "75": 32, "76": 32, "77": 32, "78": 34, "79": 35, "80": 35, "81": 35, "82": 37, "83": 38, "84": 38, "85": 38, "86": 40, "87": 41, "88": 41, "89": 41, "90": 44, "91": 44, "92": 44, "97": 49, "98": 56, "99": 56, "104": 59, "105": 61, "106": 62, "107": 63, "108": 63, "109": 68, "110": 68, "111": 69, "112": 69, "113": 72, "114": 73, "115": 73, "116": 73, "117": 73, "118": 73, "119": 75, "120": 76, "121": 80, "122": 84, "123": 85, "124": 89, "125": 90, "126": 95, "131": 111, "136": 115, "141": 120, "146": 126, "151": 137, "156": 150, "157": 156, "158": 156, "159": 156, "165": 120, "176": 141, "185": 141, "186": 142, "187": 143, "188": 145, "189": 146, "190": 146, "191": 146, "192": 146, "193": 146, "194": 146, "195": 146, "196": 146, "197": 146, "198": 148, "199": 150, "205": 49, "211": 49, "217": 115, "228": 105, "234": 105, "240": 58, "246": 58, "252": 128}, "source_encoding": "utf-8", "uri": "/homepage/templates/base.htm", "filename": "C:/Users/coolp/Google Drive/IS 411 - Systems Design and Implementation/FOMO/homepage/templates/base.htm"}
__M_END_METADATA
"""

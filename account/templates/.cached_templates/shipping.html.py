# -*- coding:utf-8 -*-
from mako import runtime, filters, cache
UNDEFINED = runtime.UNDEFINED
STOP_RENDERING = runtime.STOP_RENDERING
__M_dict_builtin = dict
__M_locals_builtin = locals
_magic_number = 10
_modified_time = 1491693678.696465
_enable_loop = True
_template_filename = 'C:/Users/coolp/Google Drive/IS 411 - Systems Design and Implementation/FOMO/account/templates/shipping.html'
_template_uri = 'shipping.html'
_source_encoding = 'utf-8'
import os, os.path, re, json
import django_mako_plus
_exports = ['content', 'header', 'body_above']


def _mako_get_namespace(context, name):
    try:
        return context.namespaces[(__name__, name)]
    except KeyError:
        _mako_generate_namespaces(context)
        return context.namespaces[(__name__, name)]
def _mako_generate_namespaces(context):
    pass
def _mako_inherit(template, context):
    _mako_generate_namespaces(context)
    return runtime._inherit_from(context, '/homepage/templates/app_base.htm', _template_uri)
def render_body(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        __M_locals = __M_dict_builtin(pageargs=pageargs)
        def content():
            return render_content(context._locals(__M_locals))
        def header():
            return render_header(context._locals(__M_locals))
        form = context.get('form', UNDEFINED)
        def body_above():
            return render_body_above(context._locals(__M_locals))
        __M_writer = context.writer()
        __M_writer('\r\n\r\n')
        if 'parent' not in context._data or not hasattr(context._data['parent'], 'header'):
            context['self'].header(**pageargs)
        

        __M_writer('\r\n\r\n')
        if 'parent' not in context._data or not hasattr(context._data['parent'], 'body_above'):
            context['self'].body_above(**pageargs)
        

        __M_writer('\r\n\r\n')
        if 'parent' not in context._data or not hasattr(context._data['parent'], 'content'):
            context['self'].content(**pageargs)
        

        __M_writer('\r\n')
        return ''
    finally:
        context.caller_stack._pop_frame()


def render_content(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        def content():
            return render_content(context)
        form = context.get('form', UNDEFINED)
        __M_writer = context.writer()
        __M_writer('\r\n<div class="container col-md-12" id="shipping-form">\r\n  ')
        __M_writer(str( form ))
        __M_writer('\r\n</div>\r\n<!-- <div class="col-md-6">\r\n  <div id="confirm_address">\r\n    <a href="/catalog/checkout" class="btn btn-lg btn-primary">Confirm my address</a>\r\n  </div>\r\n</div> -->\r\n')
        return ''
    finally:
        context.caller_stack._pop_frame()


def render_header(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        def header():
            return render_header(context)
        __M_writer = context.writer()
        __M_writer('My Shipping Information')
        return ''
    finally:
        context.caller_stack._pop_frame()


def render_body_above(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        def body_above():
            return render_body_above(context)
        __M_writer = context.writer()
        __M_writer('\r\n  <ol class="breadcrumb">\r\n    <li class="breadcrumb-item"><a href="/homepage/">Homepage</a></li>\r\n    <li class="breadcrumb-item"><a href="/catalog/">Catalog</a></li>\r\n    <li class="breadcrumb-item"><a href="/account/shoppingCart">My Cart</a></li>\r\n    <li class="breadcrumb-item active">My Shipping Information</a></li>\r\n  </ol>\r\n')
        return ''
    finally:
        context.caller_stack._pop_frame()


"""
__M_BEGIN_METADATA
{"uri": "shipping.html", "source_encoding": "utf-8", "line_map": {"51": 12, "69": 14, "70": 16, "71": 16, "41": 1, "77": 3, "46": 3, "83": 3, "56": 23, "89": 5, "101": 95, "29": 0, "62": 14, "95": 5}, "filename": "C:/Users/coolp/Google Drive/IS 411 - Systems Design and Implementation/FOMO/account/templates/shipping.html"}
__M_END_METADATA
"""

# -*- coding:utf-8 -*-
from mako import runtime, filters, cache
UNDEFINED = runtime.UNDEFINED
STOP_RENDERING = runtime.STOP_RENDERING
__M_dict_builtin = dict
__M_locals_builtin = locals
_magic_number = 10
_modified_time = 1491612967.8152199
_enable_loop = True
_template_filename = 'C:/Users/coolp/Google Drive/IS 411 - Systems Design and Implementation/FOMO/account/templates/shoppingcart.html'
_template_uri = 'shoppingcart.html'
_source_encoding = 'utf-8'
import os, os.path, re, json
import django_mako_plus
_exports = ['content', 'body_above', 'header']


def _mako_get_namespace(context, name):
    try:
        return context.namespaces[(__name__, name)]
    except KeyError:
        _mako_generate_namespaces(context)
        return context.namespaces[(__name__, name)]
def _mako_generate_namespaces(context):
    pass
def _mako_inherit(template, context):
    _mako_generate_namespaces(context)
    return runtime._inherit_from(context, '/homepage/templates/app_base.htm', _template_uri)
def render_body(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        __M_locals = __M_dict_builtin(pageargs=pageargs)
        tax = context.get('tax', UNDEFINED)
        user = context.get('user', UNDEFINED)
        STATIC_URL = context.get('STATIC_URL', UNDEFINED)
        shipping = context.get('shipping', UNDEFINED)
        def header():
            return render_header(context._locals(__M_locals))
        len = context.get('len', UNDEFINED)
        myCart = context.get('myCart', UNDEFINED)
        total = context.get('total', UNDEFINED)
        def content():
            return render_content(context._locals(__M_locals))
        def body_above():
            return render_body_above(context._locals(__M_locals))
        subtotal = context.get('subtotal', UNDEFINED)
        __M_writer = context.writer()
        __M_writer('\r\n\r\n')
        if 'parent' not in context._data or not hasattr(context._data['parent'], 'header'):
            context['self'].header(**pageargs)
        

        __M_writer('\r\n\r\n')
        if 'parent' not in context._data or not hasattr(context._data['parent'], 'body_above'):
            context['self'].body_above(**pageargs)
        

        __M_writer('\r\n\r\n')
        if 'parent' not in context._data or not hasattr(context._data['parent'], 'content'):
            context['self'].content(**pageargs)
        

        __M_writer('\r\n')
        return ''
    finally:
        context.caller_stack._pop_frame()


def render_content(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        tax = context.get('tax', UNDEFINED)
        user = context.get('user', UNDEFINED)
        STATIC_URL = context.get('STATIC_URL', UNDEFINED)
        shipping = context.get('shipping', UNDEFINED)
        len = context.get('len', UNDEFINED)
        myCart = context.get('myCart', UNDEFINED)
        total = context.get('total', UNDEFINED)
        def content():
            return render_content(context)
        subtotal = context.get('subtotal', UNDEFINED)
        __M_writer = context.writer()
        __M_writer('\r\n')
        if user.is_authenticated():
            if len(myCart) > 0:
                __M_writer('  <table>\r\n    <tr>\r\n      <th></th>\r\n      <th>Name</th>\r\n      <th>Price</th>\r\n      <th>Quantity</th>\r\n      <th>Extended Amount</th>\r\n      <th></th>\r\n    </tr>\r\n')
                for item in myCart:
                    __M_writer('    <tr>\r\n      <td><a href="/catalog/product/')
                    __M_writer(str( item.product.id ))
                    __M_writer('"><img src="')
                    __M_writer(str( STATIC_URL ))
                    __M_writer('homepage/media/Images/')
                    __M_writer(str( item.product.name ))
                    __M_writer('.svg" style="width: 50px;" /></a>\r\n      <td><a href="/catalog/product/')
                    __M_writer(str( item.product.id ))
                    __M_writer('">')
                    __M_writer(str( item.product.name ))
                    __M_writer('</a></td>\r\n      <td>')
                    __M_writer(str( item.product.price ))
                    __M_writer('</td>\r\n      <td>')
                    __M_writer(str( item.quantity ))
                    __M_writer('</td>\r\n      <td>')
                    __M_writer(str( item.product.price * item.quantity ))
                    __M_writer('</td>\r\n      <td><a href="/account/shoppingCart.removeProduct/')
                    __M_writer(str( item.product.id ))
                    __M_writer('" class="btn btn-sm btn-danger" id="remove_item_')
                    __M_writer(str( item.product.id ))
                    __M_writer('">Remove Item</a></td>\r\n')
                __M_writer('  </table>\r\n    </br>\r\n    <div style="text-align: center;">\r\n      <h3>Subtotal: $')
                __M_writer(str( subtotal ))
                __M_writer('</h3>\r\n      <h3>Tax: $')
                __M_writer(str( tax ))
                __M_writer('</h3>\r\n      <h3>Shipping: $')
                __M_writer(str( shipping ))
                __M_writer('.00</h3>\r\n      <hr>\r\n      <h3>Total: $')
                __M_writer(str( total ))
                __M_writer('</h3>\r\n    </div>\r\n    <a class="btn btn-lg btn-danger" href="/account/shoppingCart.clearCart" style="margin-left: 35%;">Clear Cart</a>\r\n    <a class="btn btn-lg btn-primary" href="/account/shipping" style="margin-left: 2;">Checkout</a>\r\n')
            else:
                __M_writer('  <div style="text-align: center;">\r\n    <h2>Nothing here!</h2>\r\n    <p>Try <a href="/catalog/">shopping around</a> a little!</p>\r\n  </div>\r\n')
        else:
            __M_writer('  <h2 style="text-align: center;">Not seeing anything? Try <a href="/account/userLogin">logging in</a></h2>\r\n')
        return ''
    finally:
        context.caller_stack._pop_frame()


def render_body_above(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        def body_above():
            return render_body_above(context)
        __M_writer = context.writer()
        __M_writer('\r\n  <ol class="breadcrumb">\r\n    <li class="breadcrumb-item"><a href="/homepage/">Homepage</a></li>\r\n    <li class="breadcrumb-item"><a href="/catalog/">Catalog</a></li>\r\n    <li class="breadcrumb-item active">My Cart</a></li>\r\n  </ol>\r\n')
        return ''
    finally:
        context.caller_stack._pop_frame()


def render_header(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        def header():
            return render_header(context)
        __M_writer = context.writer()
        __M_writer('My Cart')
        return ''
    finally:
        context.caller_stack._pop_frame()


"""
__M_BEGIN_METADATA
{"source_encoding": "utf-8", "uri": "shoppingcart.html", "line_map": {"133": 5, "139": 3, "145": 3, "151": 145, "29": 0, "48": 1, "53": 3, "58": 11, "63": 54, "69": 13, "83": 13, "84": 14, "85": 15, "86": 16, "87": 25, "88": 26, "89": 27, "90": 27, "91": 27, "92": 27, "93": 27, "94": 27, "95": 28, "96": 28, "97": 28, "98": 28, "99": 29, "100": 29, "101": 30, "102": 30, "103": 31, "104": 31, "105": 32, "106": 32, "107": 32, "108": 32, "109": 34, "110": 37, "111": 37, "112": 38, "113": 38, "114": 39, "115": 39, "116": 41, "117": 41, "118": 45, "119": 46, "120": 51, "121": 52, "127": 5}, "filename": "C:/Users/coolp/Google Drive/IS 411 - Systems Design and Implementation/FOMO/account/templates/shoppingcart.html"}
__M_END_METADATA
"""

# -*- coding:utf-8 -*-
from mako import runtime, filters, cache
UNDEFINED = runtime.UNDEFINED
STOP_RENDERING = runtime.STOP_RENDERING
__M_dict_builtin = dict
__M_locals_builtin = locals
_magic_number = 10
_modified_time = 1491320800.4702842
_enable_loop = True
_template_filename = 'C:/Users/coolp/Google Drive/IS 411 - Systems Design and Implementation/FOMO/account/templates/user.html'
_template_uri = 'user.html'
_source_encoding = 'utf-8'
import os, os.path, re, json
import django_mako_plus
_exports = ['body_above', 'content']


def _mako_get_namespace(context, name):
    try:
        return context.namespaces[(__name__, name)]
    except KeyError:
        _mako_generate_namespaces(context)
        return context.namespaces[(__name__, name)]
def _mako_generate_namespaces(context):
    pass
def _mako_inherit(template, context):
    _mako_generate_namespaces(context)
    return runtime._inherit_from(context, 'app_base.htm', _template_uri)
def render_body(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        __M_locals = __M_dict_builtin(pageargs=pageargs)
        def body_above():
            return render_body_above(context._locals(__M_locals))
        form = context.get('form', UNDEFINED)
        def content():
            return render_content(context._locals(__M_locals))
        account = context.get('account', UNDEFINED)
        __M_writer = context.writer()
        __M_writer('\r\n\r\n')
        if 'parent' not in context._data or not hasattr(context._data['parent'], 'body_above'):
            context['self'].body_above(**pageargs)
        

        __M_writer('\r\n\r\n')
        if 'parent' not in context._data or not hasattr(context._data['parent'], 'content'):
            context['self'].content(**pageargs)
        

        __M_writer('\r\n')
        return ''
    finally:
        context.caller_stack._pop_frame()


def render_body_above(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        def body_above():
            return render_body_above(context)
        __M_writer = context.writer()
        __M_writer('\r\n  <ol class="breadcrumb">\r\n    <li class="breadcrumb-item"><a href="/homepage/">Homepage</a></li>\r\n    <li class="breadcrumb-item"><a href="/account/users">Users</a></li>\r\n    <li class="breadcrumb-item active">Edit User</li>\r\n  </ol>\r\n')
        return ''
    finally:
        context.caller_stack._pop_frame()


def render_content(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        form = context.get('form', UNDEFINED)
        def content():
            return render_content(context)
        account = context.get('account', UNDEFINED)
        __M_writer = context.writer()
        __M_writer('\r\n    <h1>')
        __M_writer(str( account.username ))
        __M_writer('</h1>\r\n    <a id="delete-user" href="../user.delete/')
        __M_writer(str( account.id ))
        __M_writer('" class="btn btn-danger">Delete User</a>\r\n    <a id="change-password" href="../changePassword/')
        __M_writer(str( account.username ))
        __M_writer('" class="btn btn-primary">Change Password</a>\r\n    <a href="../users/" class="btn btn-warning">Cancel</a>\r\n    ')
        __M_writer(str( form ))
        __M_writer('\r\n\r\n    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">\r\n      <div class="modal-dialog" role="document">\r\n        <div class="modal-content">\r\n          <div class="modal-header">\r\n            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>\r\n            <h4 class="modal-title" id="myModalLabel">Confirm</h4>\r\n          </div>\r\n          <div class="modal-body">\r\n            Are you sure you want to delete this user?\r\n          </div>\r\n          <div class="modal-footer">\r\n            <a id="delete-button" class="btn btn-danger">Yes</a>\r\n            <button type="button" class="btn btn-default" data-dismiss="modal">No</button>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\n')
        return ''
    finally:
        context.caller_stack._pop_frame()


"""
__M_BEGIN_METADATA
{"line_map": {"45": 9, "68": 11, "40": 1, "76": 11, "77": 12, "78": 12, "79": 13, "80": 13, "81": 14, "50": 35, "83": 16, "84": 16, "82": 14, "56": 3, "90": 84, "29": 0, "62": 3}, "uri": "user.html", "filename": "C:/Users/coolp/Google Drive/IS 411 - Systems Design and Implementation/FOMO/account/templates/user.html", "source_encoding": "utf-8"}
__M_END_METADATA
"""

# -*- coding:utf-8 -*-
from mako import runtime, filters, cache
UNDEFINED = runtime.UNDEFINED
STOP_RENDERING = runtime.STOP_RENDERING
__M_dict_builtin = dict
__M_locals_builtin = locals
_magic_number = 10
_modified_time = 1490986378.4146667
_enable_loop = True
_template_filename = 'C:/Users/coolp/Google Drive/IS 411 - Systems Design and Implementation/FOMO/account/templates/userLogin.html'
_template_uri = 'userLogin.html'
_source_encoding = 'utf-8'
import os, os.path, re, json
import django_mako_plus
_exports = ['content', 'body_above', 'header']


def _mako_get_namespace(context, name):
    try:
        return context.namespaces[(__name__, name)]
    except KeyError:
        _mako_generate_namespaces(context)
        return context.namespaces[(__name__, name)]
def _mako_generate_namespaces(context):
    pass
def _mako_inherit(template, context):
    _mako_generate_namespaces(context)
    return runtime._inherit_from(context, 'app_base.htm', _template_uri)
def render_body(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        __M_locals = __M_dict_builtin(pageargs=pageargs)
        form = context.get('form', UNDEFINED)
        def content():
            return render_content(context._locals(__M_locals))
        invalid = context.get('invalid', UNDEFINED)
        csrf_input = context.get('csrf_input', UNDEFINED)
        def body_above():
            return render_body_above(context._locals(__M_locals))
        def header():
            return render_header(context._locals(__M_locals))
        __M_writer = context.writer()
        __M_writer('\r\n\r\n')
        if 'parent' not in context._data or not hasattr(context._data['parent'], 'header'):
            context['self'].header(**pageargs)
        

        __M_writer('\r\n\r\n')
        if 'parent' not in context._data or not hasattr(context._data['parent'], 'body_above'):
            context['self'].body_above(**pageargs)
        

        __M_writer('\r\n\r\n')
        if 'parent' not in context._data or not hasattr(context._data['parent'], 'content'):
            context['self'].content(**pageargs)
        

        __M_writer('\r\n')
        return ''
    finally:
        context.caller_stack._pop_frame()


def render_content(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        form = context.get('form', UNDEFINED)
        def content():
            return render_content(context)
        invalid = context.get('invalid', UNDEFINED)
        csrf_input = context.get('csrf_input', UNDEFINED)
        __M_writer = context.writer()
        __M_writer('\r\n  <form action="" method="post">\r\n    ')
        __M_writer(str( csrf_input ))
        __M_writer('\r\n    ')
        __M_writer(str(form.as_p()))
        __M_writer('\n    <div style="text-align: right;">\r\n      <button type="submit" class="btn btn-lg btn-primary">Login</button>\r\n      <a class="btn btn-lg btn-info" href="/account/signUp">Register</a>\n    </div>\n')
        if invalid:
            __M_writer('    <p>Something is wrong.</p>\r\n')
        __M_writer('  </form>\r\n')
        return ''
    finally:
        context.caller_stack._pop_frame()


def render_body_above(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        def body_above():
            return render_body_above(context)
        __M_writer = context.writer()
        __M_writer('\r\n  <ol class="breadcrumb">\r\n    <li class="breadcrumb-item"><a href="/homepage/">Homepage</a></li>\r\n    <li class="breadcrumb-item active">Login</a></li>\r\n  </ol>\r\n')
        return ''
    finally:
        context.caller_stack._pop_frame()


def render_header(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        def header():
            return render_header(context)
        __M_writer = context.writer()
        __M_writer('Login')
        return ''
    finally:
        context.caller_stack._pop_frame()


"""
__M_BEGIN_METADATA
{"line_map": {"64": 12, "80": 23, "98": 3, "43": 1, "104": 3, "73": 12, "74": 14, "75": 14, "76": 15, "77": 15, "78": 20, "79": 21, "48": 3, "53": 10, "86": 5, "110": 104, "58": 24, "92": 5, "29": 0}, "source_encoding": "utf-8", "filename": "C:/Users/coolp/Google Drive/IS 411 - Systems Design and Implementation/FOMO/account/templates/userLogin.html", "uri": "userLogin.html"}
__M_END_METADATA
"""

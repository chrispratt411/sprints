# -*- coding:utf-8 -*-
from mako import runtime, filters, cache
UNDEFINED = runtime.UNDEFINED
STOP_RENDERING = runtime.STOP_RENDERING
__M_dict_builtin = dict
__M_locals_builtin = locals
_magic_number = 10
_modified_time = 1488652418.9454677
_enable_loop = True
_template_filename = 'C:/Users/coolp/Google Drive/IS 411 - Systems Design and Implementation/FOMO/account/templates/users.html'
_template_uri = 'users.html'
_source_encoding = 'utf-8'
import os, os.path, re, json
import django_mako_plus
_exports = ['body_above', 'content']


def _mako_get_namespace(context, name):
    try:
        return context.namespaces[(__name__, name)]
    except KeyError:
        _mako_generate_namespaces(context)
        return context.namespaces[(__name__, name)]
def _mako_generate_namespaces(context):
    pass
def _mako_inherit(template, context):
    _mako_generate_namespaces(context)
    return runtime._inherit_from(context, 'app_base.htm', _template_uri)
def render_body(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        __M_locals = __M_dict_builtin(pageargs=pageargs)
        def body_above():
            return render_body_above(context._locals(__M_locals))
        def content():
            return render_content(context._locals(__M_locals))
        accounts = context.get('accounts', UNDEFINED)
        STATIC_URL = context.get('STATIC_URL', UNDEFINED)
        __M_writer = context.writer()
        __M_writer('\r\n<link rel="stylesheet" href="')
        __M_writer(str(STATIC_URL))
        __M_writer('account/styles/users.css" />\r\n\r\n')
        if 'parent' not in context._data or not hasattr(context._data['parent'], 'body_above'):
            context['self'].body_above(**pageargs)
        

        __M_writer('\r\n\r\n')
        if 'parent' not in context._data or not hasattr(context._data['parent'], 'content'):
            context['self'].content(**pageargs)
        

        __M_writer('\r\n')
        return ''
    finally:
        context.caller_stack._pop_frame()


def render_body_above(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        def body_above():
            return render_body_above(context)
        __M_writer = context.writer()
        __M_writer('\r\n  <ol class="breadcrumb">\r\n    <li class="breadcrumb-item"><a href="/homepage/">Homepage</a></li>\r\n    <li class="breadcrumb-item active">Users</li>\r\n  </ol>\r\n')
        return ''
    finally:
        context.caller_stack._pop_frame()


def render_content(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        def content():
            return render_content(context)
        accounts = context.get('accounts', UNDEFINED)
        __M_writer = context.writer()
        __M_writer('\r\n    <h2 style="text-align: center;">Manage Users</h2>\r\n\r\n    <table class="table table-striped">\r\n      <tr>\r\n        <th>Username</th>\r\n        <th>First Name</th>\r\n        <th>Last Name</th>\r\n        <th>Email</th>\r\n        <th>City</th>\r\n        <th>State</th>\r\n        <th>Birthdate</th>\r\n        <th><a href=\'/account/create\' class=\'btn btn-primary\'>Create User</a></th>\r\n      </tr>\r\n')
        for p in accounts:
            __M_writer('      <tr>\r\n        <td>')
            __M_writer(str( p.username ))
            __M_writer('</td>\r\n        <td>')
            __M_writer(str( p.first_name ))
            __M_writer('</td>\r\n        <td>')
            __M_writer(str( p.last_name ))
            __M_writer('</td>\r\n        <td>')
            __M_writer(str( p.email ))
            __M_writer('</td>\r\n        <td>')
            __M_writer(str( p.city ))
            __M_writer('</td>\r\n        <td>')
            __M_writer(str( p.state ))
            __M_writer('</td>\r\n        <td>')
            __M_writer(str( p.birthdate ))
            __M_writer('</td>\r\n        <td>\r\n          <a href="/account/user/')
            __M_writer(str(p.id))
            __M_writer('">Edit</a>\r\n          |\r\n          <a class="change-password" href="/account/changePassword/')
            __M_writer(str(p.username))
            __M_writer('">Change Password</a>\r\n          |\r\n          <a href="/account/user.delete/')
            __M_writer(str(p.id))
            __M_writer('" class="delete-user">Delete</a>\r\n      </tr>\r\n')
        __M_writer('    </table>\r\n\r\n    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">\r\n      <div class="modal-dialog" role="document">\r\n        <div class="modal-content">\r\n          <div class="modal-header">\r\n            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>\r\n            <h4 class="modal-title" id="myModalLabel">Confirm</h4>\r\n          </div>\r\n          <div class="modal-body">\r\n            Are you sure you want to delete this user?\r\n          </div>\r\n          <div class="modal-footer">\r\n            <a id="delete-button" class="btn btn-danger">Yes</a>\r\n            <button type="button" class="btn btn-default" data-dismiss="modal">No</button>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n')
        return ''
    finally:
        context.caller_stack._pop_frame()


"""
__M_BEGIN_METADATA
{"filename": "C:/Users/coolp/Google Drive/IS 411 - Systems Design and Implementation/FOMO/account/templates/users.html", "source_encoding": "utf-8", "line_map": {"64": 4, "70": 11, "77": 11, "78": 25, "79": 26, "80": 27, "81": 27, "82": 28, "83": 28, "84": 29, "85": 29, "86": 30, "87": 30, "88": 31, "89": 31, "90": 32, "91": 32, "92": 33, "29": 0, "94": 35, "95": 35, "96": 37, "97": 37, "98": 39, "99": 39, "100": 42, "40": 1, "41": 2, "42": 2, "93": 33, "47": 9, "52": 61, "58": 4, "106": 100}, "uri": "users.html"}
__M_END_METADATA
"""

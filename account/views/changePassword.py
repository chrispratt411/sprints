from django.conf import settings
from django_mako_plus import view_function
from datetime import datetime
from .. import dmp_render, dmp_render_to_string
from account import models as amod
from django import forms
from formlib.form import FormMixIn
from django.http import HttpResponseRedirect
from django.contrib.auth.forms import ReadOnlyPasswordHashField
from django.contrib.auth import authenticate, login, get_user

@view_function
def process_request(request):
    try:
        account = amod.FomoUser.objects.get(username=request.urlparams[0])
        print('>>>>>> account exists')
        print('>>>>>> ', account.first_name)
    except amod.FomoUser.DoesNotExist:
        return HttpResponseRedirect('/account/')

    user = request.user
    realHistory = []

    if user.is_authenticated():
        for h in amod.History.objects.filter(user=user).order_by('-id'):
          if h.product not in realHistory:
              realHistory.append(h.product)
              if len(realHistory) >= 5:
                  break

    form = PasswordChangeForm(request, account=account, initial={
        'username': account.username,
    })

    if form.is_valid():
        print('>>>>> form is valid')
        passwordReal = form.cleaned_data['password']
        user = authenticate(username=request.user.username, password=passwordReal)
        login(request, user)
        form.commit(account)
        return HttpResponseRedirect('/account/')

    context = {
        'account': account,
        'form': form,
        'realHistory': realHistory,
    }

    return dmp_render(request, 'changePassword.html', context)

class PasswordChangeForm(FormMixIn, forms.Form):

    def init(self, account):
        self.fields['username'] = forms.CharField(label='Username', max_length=100, widget=forms.TextInput(attrs={'readonly':'readonly'}))
        self.fields['old_password'] = forms.CharField(label='Old Password', max_length=100)
        self.fields['password'] = forms.CharField(widget=forms.PasswordInput, label="Password")
        self.fields['password2'] = forms.CharField(widget=forms.PasswordInput, label="Type password again")
        print('>>>>>> entered passwordchange')

    def clean(self):
        password = self.cleaned_data.get('password')
        old_password = self.cleaned_data.get('old_password')
        username = self.cleaned_data.get('username')
        password2 = self.cleaned_data.get('password2')

        print('>>>>>>', password2)

        account = amod.FomoUser.objects.get(username=username)
        user = authenticate(username=username, password=old_password)
        if user is None:
            raise forms.ValidationError("Old password is incorrect")

        if password != password2:
            print('>>>>>>>>>>>> Passwords don\'t match')
            raise forms.ValidationError("Passwords don't match")

        return self.cleaned_data

    def commit(self, account):
        account.username = self.cleaned_data.get('username')
        print('old password changing to ', self.cleaned_data.get('password'))
        account.set_password(self.cleaned_data.get('password'))
        account.save()

from django.conf import settings
from django_mako_plus import view_function
from datetime import datetime
from .. import dmp_render, dmp_render_to_string
from account import models as cmod
from django import forms
from formlib.form import FormMixIn
from django.http import HttpResponseRedirect
from django.core.exceptions import ValidationError
from django.contrib.auth.decorators import login_required, user_passes_test, permission_required
from datetime import datetime
from django.contrib.auth.models import Permission, Group
from django.contrib.auth import authenticate, login, get_user

@view_function
@permission_required('account.add_fomouser')
def process_request(request):
    # if method is POST, save data to product
    form = CreateUserForm(request)

    if form.is_valid():
        print('>>>>>form is valid')
        print(form.cleaned_data['first_name'])
        user = cmod.FomoUser()
        form.commit(user)
        return HttpResponseRedirect('/account/users/')

    context = {
        'form': form,
    }
    return dmp_render(request, 'create.html', context)

states = [
    ('Alabama', 'AL'),
    ('Alaska', 'AK'),
    ('Arizona', 'AZ'),
    ('Arkansas', 'AR'),
    ('California', 'CA'),
    ('Colorado', 'CO'),
    ('Connecticut', 'CT'),
    ('Delaware', 'DE'),
    ('Florida', 'FL'),
    ('Georgia', 'GA'),
    ('Hawaii', 'HI'),
    ('Idaho', 'ID'),
    ('Illinois', 'IL'),
    ('Indiana', 'IN'),
    ('Iowa', 'IA'),
    ('Kansas', 'KS'),
    ('Kentucky', 'KY'),
    ('Louisiana', 'LA'),
    ('Maine', 'ME'),
    ('Maryland', 'MD'),
    ('Massachusetts', 'MA'),
    ('Michigan', 'MI'),
    ('Minnesota', 'MN'),
    ('Mississippi', 'MS'),
    ('Missouri', 'MO'),
    ('Montana', 'MT'),
    ('Nebraska', 'NE'),
    ('Nevada', 'NV'),
    ('New Hampshire', 'NH'),
    ('New Jersey', 'NJ'),
    ('New Mexico', 'NM'),
    ('New York', 'NY'),
    ('North Carolina', 'NC'),
    ('North Dakota', 'ND'),
    ('Ohio', 'OH'),
    ('Oklahoma', 'OK'),
    ('Oregon', 'OR'),
    ('Pennsylvania', 'PA'),
    ('Rhode Island', 'RI'),
    ('South Carolina', 'SC'),
    ('South Dakota', 'SD'),
    ('Tennessee', 'TN'),
    ('Texas', 'TX'),
    ('Utah', 'UT'),
    ('Vermont', 'VT'),
    ('Virginia', 'VA'),
    ('Washington', 'WA'),
    ('West Virginia', 'WV'),
    ('Wisconsin', 'WI'),
    ('Wyoming', 'WY'),
]

permissions = [
    ("add_fomouser", "Can add users"),
    ("change_fomouser", "Can edit individual users"),
    ("delete_fomouser", "Can delete users"),
]

class CreateUserForm(FormMixIn, forms.Form):

    def init(self, user):
        self.fields['first_name'] = forms.CharField(label='First Name', max_length=100)
        self.fields['last_name'] = forms.CharField(label='Last Name', max_length=100)
        self.fields['username'] = forms.CharField(label='Username')
        self.fields['password'] = forms.CharField(widget=forms.PasswordInput, label="Password")
        self.fields['password2'] = forms.CharField(widget=forms.PasswordInput, label="Type password again")
        self.fields['birthdate'] = forms.CharField(label='Birthdate')
        self.fields['email'] = forms.EmailField(label='Email')
        self.fields['street_address'] = forms.CharField(label='Street Address')
        self.fields['city'] = forms.CharField(label='City')
        self.fields['state'] = forms.ChoiceField(label='State', choices=states)
        self.fields['zip_code'] = forms.CharField(label='Zip')
        self.fields['phone'] = forms.CharField(label='Phone')
        self.fields['permissions'] = forms.MultipleChoiceField(label='Permissions', choices=permissions)

    # def clean_name(self):
    #     name = self.cleaned_data.get('name')
    #     parts = name.split()
    #     if len(parts)

    def clean(self):
        password = self.cleaned_data.get('password')
        password2 = self.cleaned_data.get('password2')
        if password2 != password:
            raise ValidationError("Passwords don't match")
        else:
            return self.cleaned_data

    def clean_username(self):
        username = self.cleaned_data.get('username')
        try:
            account = cmod.FomoUser.objects.get(username=username)
            print('>>>> account exists')
            raise ValidationError("Username already exists.")
        except cmod.FomoUser.DoesNotExist:
            return username

    def commit(self, user):
        user.first_name = self.cleaned_data.get('first_name')
        user.last_name = self.cleaned_data.get('last_name')
        user.username = self.cleaned_data.get('username')
        user.set_password(self.cleaned_data.get('password'))
        user.email = self.cleaned_data.get('email')
        user.street_address = self.cleaned_data.get('street_address')
        user.city = self.cleaned_data.get('city')
        user.state = self.cleaned_data.get('state')
        user.zip_code = self.cleaned_data.get('zip_code')
        user.phone = self.cleaned_data.get('phone')
        user.birthdate = self.cleaned_data.get('birthdate')
        user.save()
        print('>>>>>>', self.cleaned_data.get('permissions'))
        g2 = Group()
        g2.name = user.username
        g2.save()

        for p in self.cleaned_data.get('permissions'):
            test = Permission.objects.get(codename=p)
            print('********************', p)
            g2.permissions.add(test)
        user.groups.add(g2)
        user.save()

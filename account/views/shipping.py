from django_mako_plus import view_function
from .. import dmp_render
from account import models as amod
from catalog import models as cmod
from django.http import HttpResponseRedirect
from django.contrib.auth.decorators import login_required
from django import forms
from django.conf import settings
from formlib.form import FormMixIn
import requests
import googlemaps

@view_function
def process_request(request):
    request.session['use_google'] = False
    user = request.user
    realHistory = []
    if user.is_authenticated():
        for h in amod.History.objects.filter(user=user).order_by('-id'):
            if h.product not in realHistory:
                realHistory.append(h.product)
                if len(realHistory) >= 5:
                    break
    else:
        return HttpResponseRedirect('/account/userLogin')

    myCart = amod.ShoppingCartItem.objects.filter(user=user)
    products = cmod.Product.objects.all()

    request.session.setdefault('full_name', '')
    request.session.setdefault('street_address', '')
    request.session.setdefault('city', '')
    request.session.setdefault('state', '')
    request.session.setdefault('postal_code', '')

    form = ShippingForm(request, initial = {
        'full_name': request.session['full_name'],
        'street_address': request.session['street_address'],
        'city': request.session['city'],
        'state': request.session['state'],
        'postal_code': request.session['postal_code'],
    })

    if form.is_valid():
        form.commit()
        return HttpResponseRedirect('/account/shippingConfirmation')

    context = {
        'myCart': myCart,
        'products': products,
        'form': form,
        'realHistory': realHistory,
    }

    return dmp_render(request, 'shipping.html', context)

states = [
    ('Alabama', 'AL'),
    ('Alaska', 'AK'),
    ('Arizona', 'AZ'),
    ('Arkansas', 'AR'),
    ('California', 'CA'),
    ('Colorado', 'CO'),
    ('Connecticut', 'CT'),
    ('Delaware', 'DE'),
    ('Florida', 'FL'),
    ('Georgia', 'GA'),
    ('Hawaii', 'HI'),
    ('Idaho', 'ID'),
    ('Illinois', 'IL'),
    ('Indiana', 'IN'),
    ('Iowa', 'IA'),
    ('Kansas', 'KS'),
    ('Kentucky', 'KY'),
    ('Louisiana', 'LA'),
    ('Maine', 'ME'),
    ('Maryland', 'MD'),
    ('Massachusetts', 'MA'),
    ('Michigan', 'MI'),
    ('Minnesota', 'MN'),
    ('Mississippi', 'MS'),
    ('Missouri', 'MO'),
    ('Montana', 'MT'),
    ('Nebraska', 'NE'),
    ('Nevada', 'NV'),
    ('New Hampshire', 'NH'),
    ('New Jersey', 'NJ'),
    ('New Mexico', 'NM'),
    ('New York', 'NY'),
    ('North Carolina', 'NC'),
    ('North Dakota', 'ND'),
    ('Ohio', 'OH'),
    ('Oklahoma', 'OK'),
    ('Oregon', 'OR'),
    ('Pennsylvania', 'PA'),
    ('Rhode Island', 'RI'),
    ('South Carolina', 'SC'),
    ('South Dakota', 'SD'),
    ('Tennessee', 'TN'),
    ('Texas', 'TX'),
    ('Utah', 'UT'),
    ('Vermont', 'VT'),
    ('Virginia', 'VA'),
    ('Washington', 'WA'),
    ('West Virginia', 'WV'),
    ('Wisconsin', 'WI'),
    ('Wyoming', 'WY'),
]

class ShippingForm(FormMixIn, forms.Form):

    form_submit = 'Confirm my address'

    def init(self):
        self.fields['full_name'] = forms.CharField(max_length=100, widget=forms.TextInput(attrs={ 'class': 'form-control', 'placeholder': 'Enter first and last name here....' }))
        self.fields['street_address'] = forms.CharField(max_length=100, widget=forms.TextInput(attrs={ 'class': 'form-control', 'placeholder': 'Enter street address here....' }))
        self.fields['city'] = forms.CharField(max_length=30, widget=forms.TextInput(attrs={ 'class': 'form-control', 'placeholder': 'Enter city here....' }))
        self.fields['state'] = forms.ChoiceField(label='State', choices=states, initial='Alabama', widget=forms.Select(attrs={ 'class': 'form-control' }))
        self.fields['postal_code'] = forms.CharField(max_length=5, min_length=5, widget=forms.TextInput(attrs={ 'class': 'form-control', 'placeholder': 'Enter zip code here....' }))

    def commit(self):
        self.request.session['full_name'] = self.cleaned_data.get('full_name')
        self.request.session['street_address'] = self.cleaned_data.get('street_address')
        self.request.session['city'] = self.cleaned_data.get('city')
        self.request.session['state'] = self.cleaned_data.get('state')
        self.request.session['postal_code'] = self.cleaned_data.get('postal_code')

    def clean(self):
        street_address = self.cleaned_data.get('street_address')
        full_name = self.cleaned_data.get('full_name')
        city = self.cleaned_data.get('city')
        state = self.cleaned_data.get('state')
        postal_code = self.cleaned_data.get('postal_code')

        # gmaps = googlemaps.Client(key=settings.GOOGLE_SERVER_KEY)
        # google_address = gmaps.geocode(street_address + ' ' + city + ', ' + state + ' ' + postal_code)
        # print('GOOGLE_ADDRESS = ', google_address)

        response = requests.get('https://maps.googleapis.com/maps/api/geocode/json', params={ 'address': street_address + ' ' + city + ', ' + state + ' ' + postal_code, 'key': settings.GOOGLE_SERVER_KEY })
        google_street_number = response.json()['results'][0]['address_components'][0]['long_name']
        google_street = response.json()['results'][0]['address_components'][1]['long_name']
        google_street_address = google_street_number + ' ' + google_street
        google_city = response.json()['results'][0]['address_components'][2]['long_name']
        google_postal_code = response.json()['results'][0]['address_components'][6]['long_name']
        google_state = state
        google_postal_code = postal_code

        if response.status_code == 200:
            if street_address != google_street_address:
                self.request.session['full_name'] = full_name
                self.request.session['google_street_address'] = google_street_address
                self.request.session['google_city'] = google_city
                self.request.session['google_state'] = google_state
                self.request.session['google_postal_code'] = google_postal_code
                self.request.session['use_google'] = True

        return self.cleaned_data

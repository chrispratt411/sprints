from django_mako_plus import view_function
from .. import dmp_render
from account import models as amod
from catalog import models as cmod
from django.http import HttpResponseRedirect
from django.contrib.auth.decorators import login_required
from django import forms
from formlib.form import FormMixIn

@view_function
def process_request(request):
    realHistory = []
    user = request.user
    if user.is_authenticated():
        for h in amod.History.objects.filter(user=user).order_by('-id'):
            if h.product not in realHistory:
                realHistory.append(h.product)
                if len(realHistory) >= 5:
                    break
    else:
        return HttpResponseRedirect('account/userLogin')

    if request.session['use_google'] == False:
        form = ShippingForm(request, initial = {
            'full_name': request.session['full_name'],
            'street_address': request.session['street_address'],
            'city': request.session['city'],
            'state': request.session['state'],
            'postal_code': request.session['postal_code'],
        })
    else:
        form = ShippingForm(request, initial = {
            'full_name': request.session['full_name'],
            'street_address': request.session['google_street_address'],
            'city': request.session['google_city'],
            'state': request.session['google_state'],
            'postal_code': request.session['google_postal_code'],
        })

    context = {
        'form': form,
        'realHistory': realHistory,
    }

    return dmp_render(request, 'shippingConfirmation.html', context)

states = [
    ('Alabama', 'AL'),
    ('Alaska', 'AK'),
    ('Arizona', 'AZ'),
    ('Arkansas', 'AR'),
    ('California', 'CA'),
    ('Colorado', 'CO'),
    ('Connecticut', 'CT'),
    ('Delaware', 'DE'),
    ('Florida', 'FL'),
    ('Georgia', 'GA'),
    ('Hawaii', 'HI'),
    ('Idaho', 'ID'),
    ('Illinois', 'IL'),
    ('Indiana', 'IN'),
    ('Iowa', 'IA'),
    ('Kansas', 'KS'),
    ('Kentucky', 'KY'),
    ('Louisiana', 'LA'),
    ('Maine', 'ME'),
    ('Maryland', 'MD'),
    ('Massachusetts', 'MA'),
    ('Michigan', 'MI'),
    ('Minnesota', 'MN'),
    ('Mississippi', 'MS'),
    ('Missouri', 'MO'),
    ('Montana', 'MT'),
    ('Nebraska', 'NE'),
    ('Nevada', 'NV'),
    ('New Hampshire', 'NH'),
    ('New Jersey', 'NJ'),
    ('New Mexico', 'NM'),
    ('New York', 'NY'),
    ('North Carolina', 'NC'),
    ('North Dakota', 'ND'),
    ('Ohio', 'OH'),
    ('Oklahoma', 'OK'),
    ('Oregon', 'OR'),
    ('Pennsylvania', 'PA'),
    ('Rhode Island', 'RI'),
    ('South Carolina', 'SC'),
    ('South Dakota', 'SD'),
    ('Tennessee', 'TN'),
    ('Texas', 'TX'),
    ('Utah', 'UT'),
    ('Vermont', 'VT'),
    ('Virginia', 'VA'),
    ('Washington', 'WA'),
    ('West Virginia', 'WV'),
    ('Wisconsin', 'WI'),
    ('Wyoming', 'WY'),
]

class ShippingForm(FormMixIn, forms.Form):

    form_submit = 'Confirm my address'

    def init(self):
        self.fields['full_name'] = forms.CharField(max_length=100, widget=forms.TextInput(attrs={ 'class': 'form-control', 'placeholder': 'Enter first and last name here....', 'readonly': 'readonly' }))
        self.fields['street_address'] = forms.CharField(max_length=100, widget=forms.TextInput(attrs={ 'class': 'form-control', 'placeholder': 'Enter street address here....', 'readonly': 'readonly' }))
        self.fields['city'] = forms.CharField(max_length=30, widget=forms.TextInput(attrs={ 'class': 'form-control', 'placeholder': 'Enter city here....', 'readonly': 'readonly' }))
        self.fields['state'] = forms.ChoiceField(label='State', choices=states, initial='Utah', widget=forms.Select(attrs={ 'class': 'form-control', 'readonly': 'readonly' }))
        self.fields['postal_code'] = forms.CharField(max_length=5, min_length=5, widget=forms.TextInput(attrs={ 'class': 'form-control', 'placeholder': 'Enter zip code here....', 'readonly': 'readonly' }))

from django_mako_plus import view_function
from .. import dmp_render
from account import models as amod
from catalog import models as cmod
from django.contrib.auth.decorators import login_required
from decimal import Decimal
from account import models as account_models
from django.http import HttpResponseRedirect

@view_function
@login_required
def process_request(request):
    user = request.user
    realHistory = []
    if user.is_authenticated():
        for h in account_models.History.objects.filter(user=user).order_by('-id'):
            if h.product not in realHistory:
                realHistory.append(h.product)
                if len(realHistory) >= 5:
                    break
    else:
        return HttpResponseRedirect('/account/userLogin')

    myCart = amod.ShoppingCartItem.objects.filter(user=user)
    products = cmod.Product.objects.all()


    subtotal = user.calc_subTotal()
    tax = user.calc_Tax()
    shipping = user.calc_Shipping()
    total = user.calc_Total()

    context = {
        'myCart': myCart,
        'products': products,
        'subtotal': subtotal,
        'tax': tax,
        'shipping': shipping,
        'total': total,
        'realHistory': realHistory,
    }

    return dmp_render(request, 'shoppingCart.html', context)

@view_function
def removeProduct(request):
    myCartItem = amod.ShoppingCartItem.objects.filter(user = request.user, product_id = request.urlparams[0])
    myCartItem.delete()
    return HttpResponseRedirect('/account/shoppingCart')

@view_function
def clearCart(request):
    myCart = request.user.shoppingCart()
    myCart.delete()

    return HttpResponseRedirect('/account/shoppingCart')

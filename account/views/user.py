from django.conf import settings
from django_mako_plus import view_function
from datetime import datetime
from .. import dmp_render, dmp_render_to_string
from account import models as cmod
from django import forms
from formlib.form import FormMixIn
from django.http import HttpResponseRedirect
from django.core.exceptions import ValidationError
from django.contrib.auth.decorators import permission_required

@view_function
@permission_required('account.change_fomouser')
def process_request(request):
    # pull all products from the DB
    try:
        account = cmod.FomoUser.objects.get(id=request.urlparams[0])
        print('>>>> account exists')
    except cmod.FomoUser.DoesNotExist:
        print('>>>> account doesn\'t exist')
        return HttpResponseRedirect('/users/')

    user = request.user
    realHistory = []

    if user.is_authenticated():
        for h in cmod.History.objects.filter(user=user).order_by('-id'):
          if h.product not in realHistory:
              realHistory.append(h.product)
              if len(realHistory) >= 5:
                  break


    form = EditUserForm(request, account=account, initial={
        'username': account.username,
        'first_name': account.first_name,
        'last_name': account.last_name,
        'email': account.email,
        'street_address': account.street_address,
        'city': account.city,
        'state': account.state,
        'zip_code': account.zip_code,
        'phone': account.phone,
        'birthdate': account.birthdate,
    })

    if form.is_valid():
        print('>>>>> form is valid')
        print(form.cleaned_data['username'])
        form.commit(account)
        return HttpResponseRedirect('/account/users/')

    context = {
        'account': account,
        'form': form,
        'realHistory': realHistory,
    }
    return dmp_render(request, 'user.html', context)

class EditUserForm(FormMixIn, forms.Form):

    def init(self, account):
        self.fields['username'] = forms.CharField(label='Username', max_length=100)
        self.fields['first_name'] = forms.CharField(label='First Name', max_length=100)
        self.fields['last_name'] = forms.CharField(label='Last Name', max_length=100)
        self.fields['birthdate'] = forms.DateField(label='Birthdate')
        self.fields['email'] = forms.EmailField(label='Email', max_length=100)
        self.fields['street_address'] = forms.CharField(label='Street Address', max_length=100)
        self.fields['city'] = forms.CharField(label='City', max_length=100)
        self.fields['state'] = forms.CharField(label='State', max_length=100)
        self.fields['zip_code'] = forms.CharField(label='Zip', max_length=100)
        self.fields['phone'] = forms.CharField(label='Phone', max_length=100)



    def commit(self, account):
        try:
            accountTest = cmod.FomoUser.objects.get(username=self.cleaned_data.get('username'))
            return HttpResponseRedirect('/users/')
        except cmod.FomoUser.DoesNotExist:
            pass
        account.username = self.cleaned_data.get('username')
        account.first_name = self.cleaned_data.get('first_name')
        account.last_name = self.cleaned_data.get('last_name')
        account.email = self.cleaned_data.get('email')
        account.street_address = self.cleaned_data.get('street_address')
        account.city = self.cleaned_data.get('city')
        account.state = self.cleaned_data.get('state')
        account.zip_code = self.cleaned_data.get('zip_code')
        account.phone = self.cleaned_data.get('phone')
        account.birthdate = self.cleaned_data.get('birthdate')
        account.save()

########################### delete method ###############################
@view_function
@permission_required('account.delete_fomouser')
def delete(request):
    try:
        account = cmod.FomoUser.objects.get(id=request.urlparams[0])
    except cmod.FomoUser.DoesNotExist:
        return HttpResponseRedirect('/account/users/')

    account.delete()
    return HttpResponseRedirect('/account/users/')

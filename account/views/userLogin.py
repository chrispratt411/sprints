from django.http import HttpResponseRedirect, HttpResponse
from django_mako_plus import view_function
from account import models as account_models
from django.contrib.auth import authenticate, login, get_user
from django.core.exceptions import ValidationError
from django import forms
from .. import dmp_render

@view_function
def process_request(request):
    user = request.user
    realHistory = []

    if user.is_authenticated():
        for h in account_models.History.objects.filter(user=user).order_by('-id'):
          if h.product not in realHistory:
              realHistory.append(h.product)
              if len(realHistory) >= 5:
                  break

    if request.method == 'POST':
        form = LoginForm(request.POST)
        print('>>>>Form is working')

        if(form.is_valid()):
            print('>>>>>', form.cleaned_data)
            username = form.cleaned_data.get('username')
            password = form.cleaned_data.get('password')
            user = authenticate(username=username, password=password)
            if user is not None:
                login(request, user)
            context = {
            'username': username,
            }
            print('>>>>>>>>>>>>>', 'authenticated')
            return HttpResponseRedirect('/homepage/index/', context)

    else:
        form = LoginForm()

    return dmp_render(request, 'userLogin.html', {
        'form':form,
        'realHistory': realHistory,
    })

class LoginForm(forms.Form):

    username = forms.CharField(label='Username', max_length=100,
        widget=forms.TextInput(attrs={ 'class': 'form-control' }))
    password = forms.CharField(label='Password', max_length=100,
        widget=forms.PasswordInput(attrs={ 'class': 'form-control' }))

    # def clean(self):
    #     username = self.cleaned_data.get('username')
    #     password = self.cleaned_data.get('password')
    #     try:
    #         user = authenticate(username=username, password=password)
    #     except catalog_models.FomoUser.DoesNotExist:
    #         print('>>>>>>>>>>>>>', 'not authenticated')
    #         raise ValidationError('Username or password is incorrect')

@view_function
def modal(request):

    if request.method == 'POST':
        form = LoginForm(request.POST)
        print('>>>>Form is working')

        if(form.is_valid()):
            print('>>>>>', form.cleaned_data)
            username = form.cleaned_data.get('username')
            password = form.cleaned_data.get('password')
            user = authenticate(username=username, password=password)
            print('>>>>>>>>>>>>>', username)
            if user is not None:
                login(request, user)
                context = {
                'username': username,
                }
                print('>>>>>>>>>>>>>', 'authenticated')
                return HttpResponseRedirect('/homepage/index/', context)
            print('>>>>>>>>>>>>>', 'not authenticated')
            raise ValidationError("Password is incorrect.")

    else:
        form = LoginForm()

    return dmp_render(request, 'userLoginModal.html', {
        'form':form,
    })

from django.conf import settings
from django_mako_plus import view_function
from datetime import datetime
from .. import dmp_render, dmp_render_to_string
from account import models as account_models
from django import forms
from formlib.form import FormMixIn
from django.contrib.auth.decorators import permission_required

@view_function
@permission_required('account.change_fomouser')
def process_request(request):
    user = request.user
    realHistory = []

    if user.is_authenticated():
        for h in account_models.History.objects.filter(user=user).order_by('-id'):
          if h.product not in realHistory:
              realHistory.append(h.product)
              if len(realHistory) >= 5:
                  break

    accounts = account_models.FomoUser.objects.order_by('username').all()

    context = {
        'accounts': accounts,
        'realHistory': realHistory,
    }
    return dmp_render(request, 'users.html', context)

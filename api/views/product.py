from django_mako_plus import view_function
from django.http import HttpResponse, HttpResponseNotFound, JsonResponse
from catalog import models as cmod
import json

@view_function
def process_request(request):
    try:
        min_price = request.GET.get('min_price')
        max_price = request.GET.get('max_price')
        category = request.GET.get('category')
        name = request.GET.get('name')
        quantity = request.GET.get('quantity')

        qry = cmod.Product.objects.all()

        if name is not None:
            qry = qry.filter(name__icontains=name)
        if category is not None:
            qry = qry.filter(category__name__icontains=category)
        if min_price is not None:
            qry = qry.filter(price__gte=min_price)
        if max_price is not None:
            qry = qry.filter(price__lte=max_price)

        products = list(qry.values('name', 'category', 'price'))

        if len(products) < 1:
            products = 'No products match description'
            return JsonResponse(products, safe=False)
    except:
        products = 'Invalid information'

    return JsonResponse(products, safe=False)

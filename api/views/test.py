import requests

url = "http://localhost:8000/api/product?name=exp&category=a"
data = requests.get(url)
print(data.text)

url = "http://localhost:8000/api/product?name=t&category=t&max_price=200"
data = requests.get(url)
print(data.text)

url = "http://localhost:8000/api/product?min_price=400"
data = requests.get(url)
print(data.text)

url = "http://localhost:8000/api/product?name=exp&max_price=asdf"
data = requests.get(url)
print(data.text)

# -*- coding: utf-8 -*-
# Generated by Django 1.10.5 on 2017-04-03 14:09
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('catalog', '0004_product_description'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='productpicture',
            name='product',
        ),
        migrations.RemoveField(
            model_name='bulkproduct',
            name='quantity',
        ),
        migrations.AddField(
            model_name='product',
            name='quantity',
            field=models.IntegerField(default=0),
        ),
        migrations.DeleteModel(
            name='ProductPicture',
        ),
    ]

from django.db import models
from polymorphic.models import PolymorphicModel

class Category(models.Model):
    name = models.TextField(blank=True, null=True)
    code = models.TextField(blank=True, null=True)

    def __str__(self):
        return self.name

class Product(PolymorphicModel):
    price = models.DecimalField(max_digits=8, decimal_places=2, blank=True, null=True) # 999,999.99
    name = models.TextField(blank=True, null=True)
    brand = models.TextField(blank=True, null=True)
    category = models.ForeignKey('Category')
    date_created = models.DateTimeField(auto_now_add=True)
    date_modified = models.DateTimeField(auto_now=True)
    description = models.TextField(blank=True, null=True)
    quantity = models.IntegerField(default=1)
    available = models.BooleanField(default=True)

class BulkProduct(Product):
    reorder_point = models.IntegerField(default=0)
    reorder_amount = models.IntegerField(default=0)

class UniqueProduct(Product):
    serial_number = models.TextField(blank=True, null=True)

class RentalProduct(Product):
    serial_number = models.TextField()

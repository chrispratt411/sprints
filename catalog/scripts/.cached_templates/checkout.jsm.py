# -*- coding:utf-8 -*-
from mako import runtime, filters, cache
UNDEFINED = runtime.UNDEFINED
STOP_RENDERING = runtime.STOP_RENDERING
__M_dict_builtin = dict
__M_locals_builtin = locals
_magic_number = 10
_modified_time = 1491678690.5003667
_enable_loop = True
_template_filename = 'C:/Users/coolp/Google Drive/IS 411 - Systems Design and Implementation/FOMO/catalog/scripts/checkout.jsm'
_template_uri = 'checkout.jsm'
_source_encoding = 'utf-8'
import os, os.path, re, json
import django_mako_plus
_exports = []


def render_body(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        __M_locals = __M_dict_builtin(pageargs=pageargs)
        __M_writer = context.writer()
        __M_writer('$(function() {\r\n\r\n  var form = $(\'#content > form\')\r\n\r\n  var handler = StripeCheckout.configure({\r\n    key: \'pk_test_MZzC6b7GXT65zTW6HeRVfiEF\',\r\n    image: \'https://stripe.com/img/documentation/checkout/marketplace.png\',\r\n    locale: \'auto\',\r\n    token: function(token) {\r\n      $(\'#id_stripe_token\').val(token.id);\r\n      form.submit();\r\n    }\r\n  });\n\n  var total = $(\'#total\').text().replace("Total: $", "");\n  console.log(parseFloat(total).toFixed(2));\n\n  form.submit(function(e) {\r\n    if ($(\'#id_stripe_token\').val() != \'\'){\r\n      return;\r\n    };\r\n    console.log(\'>>>>>>Worked\');\r\n    handler.open({\r\n      name: \'Family Oriented Music Organization\',\r\n      amount: parseFloat(total).toFixed(2) * 100\r\n    });\r\n    e.preventDefault();\r\n  });\r\n\r\n  window.addEventListener(\'popstate\', function(){\r\n    handler.close();\r\n  });\r\n\r\n});\r\n')
        return ''
    finally:
        context.caller_stack._pop_frame()


"""
__M_BEGIN_METADATA
{"uri": "checkout.jsm", "filename": "C:/Users/coolp/Google Drive/IS 411 - Systems Design and Implementation/FOMO/catalog/scripts/checkout.jsm", "source_encoding": "utf-8", "line_map": {"18": 0, "29": 23, "23": 1}}
__M_END_METADATA
"""

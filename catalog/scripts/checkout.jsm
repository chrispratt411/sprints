$(function() {

  var form = $('#content > form')

  var handler = StripeCheckout.configure({
    key: 'pk_test_MZzC6b7GXT65zTW6HeRVfiEF',
    image: 'https://stripe.com/img/documentation/checkout/marketplace.png',
    locale: 'auto',
    token: function(token) {
      $('#id_stripe_token').val(token.id);
      form.submit();
    }
  });

  var total = $('#total').text().replace("Total: $", "");
  console.log(parseFloat(total).toFixed(2));

  form.submit(function(e) {
    if ($('#id_stripe_token').val() != ''){
      return;
    };
    console.log('>>>>>>Worked');
    handler.open({
      name: 'Family Oriented Music Organization',
      amount: parseFloat(total).toFixed(2) * 100
    });
    e.preventDefault();
  });

  window.addEventListener('popstate', function(){
    handler.close();
  });

});

$(function(){
  console.log('>>>>>>>function');
  var pid = $('#productpic').attr('data-pid');

  $('#productpic').click(function() {
    console.log('############# image modal')
    $.loadmodal('/catalog/product.modal/' + pid);
  });

  var options = {
    target: '#purchase_container'
  };

  $('#buy_now_form').ajaxForm({
    target: '#purchase_container'
  });
});

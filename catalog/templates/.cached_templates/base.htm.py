# -*- coding:utf-8 -*-
from mako import runtime, filters, cache
UNDEFINED = runtime.UNDEFINED
STOP_RENDERING = runtime.STOP_RENDERING
__M_dict_builtin = dict
__M_locals_builtin = locals
_magic_number = 10
_modified_time = 1487782502.6678295
_enable_loop = True
_template_filename = 'C:/Users/coolp/Google Drive/IS 411 - Systems Design and Implementation/FOMO/catalog/templates/base.htm'
_template_uri = 'base.htm'
_source_encoding = 'utf-8'
import os, os.path, re, json
import django_mako_plus
_exports = ['header_items']


def _mako_get_namespace(context, name):
    try:
        return context.namespaces[(__name__, name)]
    except KeyError:
        _mako_generate_namespaces(context)
        return context.namespaces[(__name__, name)]
def _mako_generate_namespaces(context):
    pass
def _mako_inherit(template, context):
    _mako_generate_namespaces(context)
    return runtime._inherit_from(context, '/homepage/templates/base.htm', _template_uri)
def render_body(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        __M_locals = __M_dict_builtin(pageargs=pageargs)
        def header_items():
            return render_header_items(context._locals(__M_locals))
        request = context.get('request', UNDEFINED)
        __M_writer = context.writer()
        __M_writer('\n\n')
        if 'parent' not in context._data or not hasattr(context._data['parent'], 'header_items'):
            context['self'].header_items(**pageargs)
        

        __M_writer('\n')
        return ''
    finally:
        context.caller_stack._pop_frame()


def render_header_items(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        def header_items():
            return render_header_items(context)
        request = context.get('request', UNDEFINED)
        __M_writer = context.writer()
        __M_writer('\n  <ul class="nav navbar-nav">\n    <li class="')
        __M_writer(str( 'active' if request.dmp_router_app == 'homepage' else '' ))
        __M_writer('"><a href="/">Home</a></li>\n    <li class="')
        __M_writer(str( 'active' if request.dmp_router_page == 'about' else '' ))
        __M_writer('"><a href="/homepage/about/">About</a></li>\n    <li class="')
        __M_writer(str( 'active' if request.dmp_router_page == 'contact' else '' ))
        __M_writer('"><a href="/homepage/contact/">Contact</a></li>\n    <li class="')
        __M_writer(str( 'active' if request.dmp_router_page == 'terms' else '' ))
        __M_writer('"><a href="/homepage/terms/">Terms</a></li>\n    <li class="')
        __M_writer(str( 'active' if request.dmp_router_page == 'faq' else '' ))
        __M_writer('"><a href="/homepage/faq/">FAQ</a></li>\n    <li class="')
        __M_writer(str( 'active' if request.dmp_router_app == 'catalog' else '' ))
        __M_writer('"><a href="/catalog/">Catalog</a></li>\n  </ul>\n')
        return ''
    finally:
        context.caller_stack._pop_frame()


"""
__M_BEGIN_METADATA
{"uri": "base.htm", "line_map": {"64": 9, "65": 9, "66": 10, "67": 10, "37": 1, "73": 67, "42": 12, "61": 7, "48": 3, "55": 3, "56": 5, "57": 5, "58": 6, "59": 6, "60": 7, "29": 0, "62": 8, "63": 8}, "source_encoding": "utf-8", "filename": "C:/Users/coolp/Google Drive/IS 411 - Systems Design and Implementation/FOMO/catalog/templates/base.htm"}
__M_END_METADATA
"""

# -*- coding:utf-8 -*-
from mako import runtime, filters, cache
UNDEFINED = runtime.UNDEFINED
STOP_RENDERING = runtime.STOP_RENDERING
__M_dict_builtin = dict
__M_locals_builtin = locals
_magic_number = 10
_modified_time = 1491678690.4633558
_enable_loop = True
_template_filename = 'C:/Users/coolp/Google Drive/IS 411 - Systems Design and Implementation/FOMO/catalog/templates/checkout.html'
_template_uri = 'checkout.html'
_source_encoding = 'utf-8'
import os, os.path, re, json
import django_mako_plus
_exports = ['header', 'body_above', 'content']


def _mako_get_namespace(context, name):
    try:
        return context.namespaces[(__name__, name)]
    except KeyError:
        _mako_generate_namespaces(context)
        return context.namespaces[(__name__, name)]
def _mako_generate_namespaces(context):
    pass
def _mako_inherit(template, context):
    _mako_generate_namespaces(context)
    return runtime._inherit_from(context, '/homepage/templates/app_base.htm', _template_uri)
def render_body(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        __M_locals = __M_dict_builtin(pageargs=pageargs)
        subtotal = context.get('subtotal', UNDEFINED)
        def header():
            return render_header(context._locals(__M_locals))
        tax = context.get('tax', UNDEFINED)
        shipping = context.get('shipping', UNDEFINED)
        total = context.get('total', UNDEFINED)
        form = context.get('form', UNDEFINED)
        def body_above():
            return render_body_above(context._locals(__M_locals))
        invalid = context.get('invalid', UNDEFINED)
        csrf_input = context.get('csrf_input', UNDEFINED)
        def content():
            return render_content(context._locals(__M_locals))
        __M_writer = context.writer()
        __M_writer('\r\n\r\n')
        if 'parent' not in context._data or not hasattr(context._data['parent'], 'header'):
            context['self'].header(**pageargs)
        

        __M_writer('\r\n\r\n')
        if 'parent' not in context._data or not hasattr(context._data['parent'], 'body_above'):
            context['self'].body_above(**pageargs)
        

        __M_writer('\r\n\r\n')
        if 'parent' not in context._data or not hasattr(context._data['parent'], 'content'):
            context['self'].content(**pageargs)
        

        __M_writer('\r\n')
        return ''
    finally:
        context.caller_stack._pop_frame()


def render_header(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        def header():
            return render_header(context)
        __M_writer = context.writer()
        __M_writer('Checkout')
        return ''
    finally:
        context.caller_stack._pop_frame()


def render_body_above(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        def body_above():
            return render_body_above(context)
        __M_writer = context.writer()
        __M_writer('\r\n  <ol class="breadcrumb">\r\n    <li class="breadcrumb-item"><a href="/homepage/">Homepage</a></li>\r\n    <li class="breadcrumb-item"><a href="/catalog/">Catalog</a></li>\r\n    <li class="breadcrumb-item"><a href="/account/shoppingCart/">My Cart</a></li>\r\n    <li class="breadcrumb-item"><a href="/account/shipping/">My Shipping Information</a></li>\r\n    <li class="breadcrumb-item active">Checkout</a></li>\r\n  </ol>\r\n')
        return ''
    finally:
        context.caller_stack._pop_frame()


def render_content(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        subtotal = context.get('subtotal', UNDEFINED)
        tax = context.get('tax', UNDEFINED)
        shipping = context.get('shipping', UNDEFINED)
        total = context.get('total', UNDEFINED)
        form = context.get('form', UNDEFINED)
        invalid = context.get('invalid', UNDEFINED)
        csrf_input = context.get('csrf_input', UNDEFINED)
        def content():
            return render_content(context)
        __M_writer = context.writer()
        __M_writer('\r\n<script src="https://checkout.stripe.com/checkout.js"></script>\r\n  <form action="" method="post">\r\n    ')
        __M_writer(str( csrf_input ))
        __M_writer('\r\n    <div id="payment_form">\r\n      ')
        __M_writer(str(form.as_p()))
        __M_writer('\r\n    </div>\r\n')
        if invalid:
            __M_writer('    <p>Something is wrong.</p>\r\n')
        if subtotal != 0:
            __M_writer('      <h3>Subtotal: $')
            __M_writer(str( subtotal ))
            __M_writer('</h3>\r\n      <h3>Tax: $')
            __M_writer(str( tax ))
            __M_writer('</h3>\r\n      <h3>Shipping: $')
            __M_writer(str( shipping ))
            __M_writer('.00</h3>\r\n      <hr>\r\n      <h3 id="total">Total: $')
            __M_writer(str( total ))
            __M_writer('</h3>\r\n      <button class="btn btn-danger" id="customButton">Pay Now</button>\r\n')
        else:
            __M_writer('      <h3>You\'re not supposed to be here! Try <a href="/catalog/index">shopping around.</a></h3>\r\n')
        __M_writer('  </form>\r\n')
        return ''
    finally:
        context.caller_stack._pop_frame()


"""
__M_BEGIN_METADATA
{"uri": "checkout.html", "filename": "C:/Users/coolp/Google Drive/IS 411 - Systems Design and Implementation/FOMO/catalog/templates/checkout.html", "source_encoding": "utf-8", "line_map": {"130": 124, "68": 3, "74": 3, "80": 5, "86": 5, "121": 30, "111": 23, "92": 15, "29": 0, "116": 27, "105": 15, "106": 18, "107": 18, "108": 20, "109": 20, "110": 22, "47": 1, "112": 25, "113": 26, "114": 26, "115": 26, "52": 3, "117": 27, "118": 28, "119": 28, "120": 30, "57": 13, "122": 32, "123": 33, "124": 35, "62": 36}}
__M_END_METADATA
"""

# -*- coding:utf-8 -*-
from mako import runtime, filters, cache
UNDEFINED = runtime.UNDEFINED
STOP_RENDERING = runtime.STOP_RENDERING
__M_dict_builtin = dict
__M_locals_builtin = locals
_magic_number = 10
_modified_time = 1491354568.287405
_enable_loop = True
_template_filename = 'C:/Users/coolp/Google Drive/IS 411 - Systems Design and Implementation/FOMO/homepage/templates/app_base.htm'
_template_uri = '/homepage/templates/app_base.htm'
_source_encoding = 'utf-8'
import os, os.path, re, json
import django_mako_plus
_exports = ['header_items']


def _mako_get_namespace(context, name):
    try:
        return context.namespaces[(__name__, name)]
    except KeyError:
        _mako_generate_namespaces(context)
        return context.namespaces[(__name__, name)]
def _mako_generate_namespaces(context):
    pass
def _mako_inherit(template, context):
    _mako_generate_namespaces(context)
    return runtime._inherit_from(context, '/homepage/templates/base.htm', _template_uri)
def render_body(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        __M_locals = __M_dict_builtin(pageargs=pageargs)
        def header_items():
            return render_header_items(context._locals(__M_locals))
        manager = context.get('manager', UNDEFINED)
        request = context.get('request', UNDEFINED)
        __M_writer = context.writer()
        __M_writer('\r\n\r\n')
        if 'parent' not in context._data or not hasattr(context._data['parent'], 'header_items'):
            context['self'].header_items(**pageargs)
        

        __M_writer('\r\n')
        return ''
    finally:
        context.caller_stack._pop_frame()


def render_header_items(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        def header_items():
            return render_header_items(context)
        manager = context.get('manager', UNDEFINED)
        request = context.get('request', UNDEFINED)
        __M_writer = context.writer()
        __M_writer('\r\n')
        if manager:
            __M_writer('    <ul class="nav navbar-nav">\r\n      <li class="')
            __M_writer(str( 'active' if request.dmp_router_page == 'index' else '' ))
            __M_writer('"><a href="/">Home</a></li>\r\n      <li class="')
            __M_writer(str( 'active' if request.dmp_router_page == 'about' else '' ))
            __M_writer('"><a href="/about/">About</a></li>\r\n      <li class="')
            __M_writer(str( 'active' if request.dmp_router_page == 'contact' else '' ))
            __M_writer('"><a href="/contact/">Contact</a></li>\r\n      <li class="')
            __M_writer(str( 'active' if request.dmp_router_page == 'terms' else '' ))
            __M_writer('"><a href="/terms/">Terms</a></li>\r\n      <li class="')
            __M_writer(str( 'active' if request.dmp_router_page == 'faq' else '' ))
            __M_writer('"><a href="/faq/">FAQ</a></li>\r\n      <li class="')
            __M_writer(str( 'active' if request.dmp_router_app == 'manager' else '' ))
            __M_writer('"><a href="/manager/">Instrument Manager</a></li>\r\n      <li class="')
            __M_writer(str( 'active' if request.dmp_router_app == 'account' else '' ))
            __M_writer('"><a href="/account/create">Create Account</a></li>\r\n    </ul>\r\n')
        __M_writer('  <ul class="nav navbar-nav">\r\n    <li class="')
        __M_writer(str( 'active' if request.dmp_router_page == 'index' else '' ))
        __M_writer('"><a href="/">Home</a></li>\r\n    <li class="')
        __M_writer(str( 'active' if request.dmp_router_page == 'about' else '' ))
        __M_writer('"><a href="/about/">About</a></li>\r\n    <li class="')
        __M_writer(str( 'active' if request.dmp_router_page == 'contact' else '' ))
        __M_writer('"><a href="/contact/">Contact</a></li>\r\n    <li class="')
        __M_writer(str( 'active' if request.dmp_router_page == 'terms' else '' ))
        __M_writer('"><a href="/terms/">Terms</a></li>\r\n    <li class="')
        __M_writer(str( 'active' if request.dmp_router_page == 'faq' else '' ))
        __M_writer('"><a href="/faq/">FAQ</a></li>\r\n    <li class="')
        __M_writer(str( 'active' if request.dmp_router_app == 'catalog' else '' ))
        __M_writer('"><a href="/catalog/">Catalog</a></li>\r\n  </ul>\r\n')
        return ''
    finally:
        context.caller_stack._pop_frame()


"""
__M_BEGIN_METADATA
{"uri": "/homepage/templates/app_base.htm", "line_map": {"64": 8, "65": 8, "66": 9, "67": 9, "68": 10, "69": 10, "70": 11, "71": 11, "72": 12, "73": 12, "74": 15, "75": 16, "76": 16, "77": 17, "78": 17, "79": 18, "80": 18, "81": 19, "82": 19, "83": 20, "84": 20, "85": 21, "86": 21, "92": 86, "29": 0, "38": 1, "43": 23, "49": 3, "57": 3, "58": 4, "59": 5, "60": 6, "61": 6, "62": 7, "63": 7}, "source_encoding": "utf-8", "filename": "C:/Users/coolp/Google Drive/IS 411 - Systems Design and Implementation/FOMO/homepage/templates/app_base.htm"}
__M_END_METADATA
"""

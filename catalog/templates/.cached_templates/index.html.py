# -*- coding:utf-8 -*-
from mako import runtime, filters, cache
UNDEFINED = runtime.UNDEFINED
STOP_RENDERING = runtime.STOP_RENDERING
__M_dict_builtin = dict
__M_locals_builtin = locals
_magic_number = 10
_modified_time = 1491488774.3961537
_enable_loop = True
_template_filename = 'C:/Users/coolp/Google Drive/IS 411 - Systems Design and Implementation/FOMO/catalog/templates/index.html'
_template_uri = 'index.html'
_source_encoding = 'utf-8'
import os, os.path, re, json
import django_mako_plus
_exports = ['body_right', 'body_left', 'body_above', 'header', 'content']


def _mako_get_namespace(context, name):
    try:
        return context.namespaces[(__name__, name)]
    except KeyError:
        _mako_generate_namespaces(context)
        return context.namespaces[(__name__, name)]
def _mako_generate_namespaces(context):
    pass
def _mako_inherit(template, context):
    _mako_generate_namespaces(context)
    return runtime._inherit_from(context, 'base.htm', _template_uri)
def render_body(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        __M_locals = __M_dict_builtin(pageargs=pageargs)
        def body_left():
            return render_body_left(context._locals(__M_locals))
        def body_above():
            return render_body_above(context._locals(__M_locals))
        realHistory = context.get('realHistory', UNDEFINED)
        user = context.get('user', UNDEFINED)
        sortedproducts = context.get('sortedproducts', UNDEFINED)
        def body_right():
            return render_body_right(context._locals(__M_locals))
        STATIC_URL = context.get('STATIC_URL', UNDEFINED)
        def content():
            return render_content(context._locals(__M_locals))
        def header():
            return render_header(context._locals(__M_locals))
        __M_writer = context.writer()
        __M_writer('\n\n')
        if 'parent' not in context._data or not hasattr(context._data['parent'], 'header'):
            context['self'].header(**pageargs)
        

        __M_writer('\n\n')
        if 'parent' not in context._data or not hasattr(context._data['parent'], 'body_above'):
            context['self'].body_above(**pageargs)
        

        __M_writer('\n\n')
        if 'parent' not in context._data or not hasattr(context._data['parent'], 'body_left'):
            context['self'].body_left(**pageargs)
        

        __M_writer('\n\n')
        if 'parent' not in context._data or not hasattr(context._data['parent'], 'content'):
            context['self'].content(**pageargs)
        

        __M_writer('\n\n')
        if 'parent' not in context._data or not hasattr(context._data['parent'], 'body_right'):
            context['self'].body_right(**pageargs)
        

        __M_writer('\n')
        return ''
    finally:
        context.caller_stack._pop_frame()


def render_body_right(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        user = context.get('user', UNDEFINED)
        STATIC_URL = context.get('STATIC_URL', UNDEFINED)
        def body_right():
            return render_body_right(context)
        realHistory = context.get('realHistory', UNDEFINED)
        __M_writer = context.writer()
        __M_writer('\n')
        if user.is_authenticated():
            __M_writer('<h1>Recently viewed.</h1>\n<ul class="nav">\n')
            for p in realHistory:
                __M_writer('  <li><a style="color: #df691a; font-size: 30px;" href="../catalog/product/')
                __M_writer(str( p.id ))
                __M_writer('">')
                __M_writer(str( p.name ))
                __M_writer('<img style="width: 60px; padding-left: 10px" src="')
                __M_writer(str( STATIC_URL ))
                __M_writer('homepage/media/Images/')
                __M_writer(str( p.name ))
                __M_writer('.svg" /></a></li>\n')
            __M_writer('</ul>\n')
        return ''
    finally:
        context.caller_stack._pop_frame()


def render_body_left(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        def body_left():
            return render_body_left(context)
        __M_writer = context.writer()
        __M_writer('\n<div id="categories">\n  <ul class="nav">\n    <h1>Categories.</h1>\n    <li><a href="../catalog">All Items</a></li>\n    <li><a href="../catalog?category=3">Brass</a></li>\n    <li><a href="../catalog?category=2">Children\'s</a></li>\n    <li><a href="../catalog?category=7">Keyboard</a></li>\n    <li><a href="../catalog?category=5">Miscellaneous</a></li>\n    <li><a href="../catalog?category=8">Percussion</a></li>\n    <li><a href="../catalog?category=1">Sheet Music</a></li>\n    <li><a href="../catalog?category=4">String</a></li>\n    <li><a href="../catalog?category=6">Woodwind</a></li>\n  </ul>\n</div>\n')
        return ''
    finally:
        context.caller_stack._pop_frame()


def render_body_above(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        def body_above():
            return render_body_above(context)
        __M_writer = context.writer()
        __M_writer('\n  <ol class="breadcrumb">\n    <li class="breadcrumb-item"><a href="/homepage/">Homepage</a></li>\n    <li class="breadcrumb-item active">Catalog</li>\n  </ol>\n')
        return ''
    finally:
        context.caller_stack._pop_frame()


def render_header(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        def header():
            return render_header(context)
        __M_writer = context.writer()
        __M_writer('\n<h1>Our Catalog.</h1>\n<input placeholder="Search products here" style="width: 250px; text-align: center;margin-left: 44vh;" id="search" class="form-control">\n</input>\n')
        return ''
    finally:
        context.caller_stack._pop_frame()


def render_content(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        sortedproducts = context.get('sortedproducts', UNDEFINED)
        STATIC_URL = context.get('STATIC_URL', UNDEFINED)
        def content():
            return render_content(context)
        __M_writer = context.writer()
        __M_writer('\n<div id="products">\n')
        for p in sortedproducts:
            __M_writer('    <span class="product" id="product')
            __M_writer(str( p.id ))
            __M_writer('">\n      <a href="../catalog/product/')
            __M_writer(str( p.id ))
            __M_writer('"><img alt="product - ')
            __M_writer(str( p.name ))
            __M_writer('" src="')
            __M_writer(str( STATIC_URL ))
            __M_writer('homepage/media/Images/')
            __M_writer(str( p.name ))
            __M_writer('.svg" />\n      <p>')
            __M_writer(str( p.name ))
            __M_writer('</p></a>\n    </span>\n')
        __M_writer('</div>\n')
        return ''
    finally:
        context.caller_stack._pop_frame()


"""
__M_BEGIN_METADATA
{"filename": "C:/Users/coolp/Google Drive/IS 411 - Systems Design and Implementation/FOMO/catalog/templates/index.html", "line_map": {"131": 3, "137": 3, "143": 33, "151": 33, "152": 35, "153": 36, "154": 36, "155": 36, "156": 37, "29": 0, "158": 37, "159": 37, "160": 37, "161": 37, "162": 37, "163": 37, "164": 38, "165": 38, "166": 41, "172": 166, "157": 37, "48": 1, "53": 7, "58": 14, "63": 31, "68": 42, "73": 53, "79": 44, "88": 44, "89": 45, "90": 46, "91": 48, "92": 49, "93": 49, "94": 49, "95": 49, "96": 49, "97": 49, "98": 49, "99": 49, "100": 49, "101": 51, "107": 16, "113": 16, "119": 9, "125": 9}, "uri": "index.html", "source_encoding": "utf-8"}
__M_END_METADATA
"""

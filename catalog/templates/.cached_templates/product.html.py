# -*- coding:utf-8 -*-
from mako import runtime, filters, cache
UNDEFINED = runtime.UNDEFINED
STOP_RENDERING = runtime.STOP_RENDERING
__M_dict_builtin = dict
__M_locals_builtin = locals
_magic_number = 10
_modified_time = 1491692592.428898
_enable_loop = True
_template_filename = 'C:/Users/coolp/Google Drive/IS 411 - Systems Design and Implementation/FOMO/catalog/templates/product.html'
_template_uri = 'product.html'
_source_encoding = 'utf-8'
import os, os.path, re, json
import django_mako_plus
_exports = ['body_above', 'body_right', 'content']


def _mako_get_namespace(context, name):
    try:
        return context.namespaces[(__name__, name)]
    except KeyError:
        _mako_generate_namespaces(context)
        return context.namespaces[(__name__, name)]
def _mako_generate_namespaces(context):
    pass
def _mako_inherit(template, context):
    _mako_generate_namespaces(context)
    return runtime._inherit_from(context, 'base.htm', _template_uri)
def render_body(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        __M_locals = __M_dict_builtin(pageargs=pageargs)
        def body_right():
            return render_body_right(context._locals(__M_locals))
        user = context.get('user', UNDEFINED)
        product = context.get('product', UNDEFINED)
        hasattr = context.get('hasattr', UNDEFINED)
        STATIC_URL = context.get('STATIC_URL', UNDEFINED)
        def content():
            return render_content(context._locals(__M_locals))
        form = context.get('form', UNDEFINED)
        def body_above():
            return render_body_above(context._locals(__M_locals))
        realHistory = context.get('realHistory', UNDEFINED)
        __M_writer = context.writer()
        __M_writer('\r\n\r\n')
        if 'parent' not in context._data or not hasattr(context._data['parent'], 'body_above'):
            context['self'].body_above(**pageargs)
        

        __M_writer('\r\n\r\n')
        if 'parent' not in context._data or not hasattr(context._data['parent'], 'content'):
            context['self'].content(**pageargs)
        

        __M_writer('\r\n\r\n')
        if 'parent' not in context._data or not hasattr(context._data['parent'], 'body_right'):
            context['self'].body_right(**pageargs)
        

        __M_writer('\r\n')
        return ''
    finally:
        context.caller_stack._pop_frame()


def render_body_above(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        def body_above():
            return render_body_above(context)
        __M_writer = context.writer()
        __M_writer('\r\n  <ol class="breadcrumb">\r\n    <li class="breadcrumb-item"><a href="/homepage/">Homepage</a></li>\r\n    <li class="breadcrumb-item"><a href="/catalog/">Catalog</a></li>\r\n    <li class="breadcrumb-item active">Edit Instrument</li>\r\n  </ol>\r\n')
        return ''
    finally:
        context.caller_stack._pop_frame()


def render_body_right(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        def body_right():
            return render_body_right(context)
        STATIC_URL = context.get('STATIC_URL', UNDEFINED)
        user = context.get('user', UNDEFINED)
        realHistory = context.get('realHistory', UNDEFINED)
        __M_writer = context.writer()
        __M_writer('\n')
        if user.is_authenticated():
            __M_writer('<h2>Recently viewed.</h2>\n<ul class="nav">\n')
            for p in realHistory:
                __M_writer('  <li><a style="color: #df691a; font-size: 30px;" href="')
                __M_writer(str( p.id ))
                __M_writer('">')
                __M_writer(str( p.name ))
                __M_writer('<img style="width: 60px; padding-left: 10px" src="')
                __M_writer(str( STATIC_URL ))
                __M_writer('homepage/media/Images/')
                __M_writer(str( p.name ))
                __M_writer('.svg" /></a></li>\n')
            __M_writer('</ul>\n')
        return ''
    finally:
        context.caller_stack._pop_frame()


def render_content(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        form = context.get('form', UNDEFINED)
        product = context.get('product', UNDEFINED)
        STATIC_URL = context.get('STATIC_URL', UNDEFINED)
        def content():
            return render_content(context)
        hasattr = context.get('hasattr', UNDEFINED)
        __M_writer = context.writer()
        __M_writer('\r\n    <h1><i>')
        __M_writer(str( product.brand ))
        __M_writer('</i> ')
        __M_writer(str( product.name ))
        __M_writer('</h1>\r\n    <div class="col-md-5">\r\n      <img id="productpic" data-pid=')
        __M_writer(str( product.id ))
        __M_writer(' alt="product - ')
        __M_writer(str( product.name ))
        __M_writer('" src="')
        __M_writer(str( STATIC_URL ))
        __M_writer('homepage/media/Images/')
        __M_writer(str( product.name ))
        __M_writer('.svg" />\r\n    </div>\r\n    <div class="col-md-7">\r\n      <h3>Price:</h3> <p>')
        __M_writer(str( product.price ))
        __M_writer('</p><br>\r\n      <h3>Description:</h3> <p>')
        __M_writer(str( product.description ))
        __M_writer('</p><br>\r\n')
        if hasattr(product, 'quantity'):
            __M_writer('        <!-- <h3>Quantity:</h3>  <select class="1-100 btn-primary"></select><br><br> -->\r\n        <!-- <form><h3>Quantity:&nbsp;\t</h3><input type="number" value=1 class="" name="quantity" min="1" max="')
            __M_writer(str( product.quantity ))
            __M_writer('"></form><br> -->\r\n')
        __M_writer('      <!-- <a href="#" id="addToCart" class="btn btn-primary">Add to Cart</a>\r\n      <a href="#" class="btn btn-info">Buy Now</a> -->\n      <div id="purchase_container" class="text-center">\r\n        ')
        __M_writer(str( form ))
        __M_writer('\r\n      </div>\r\n    </div>\r\n')
        return ''
    finally:
        context.caller_stack._pop_frame()


"""
__M_BEGIN_METADATA
{"line_map": {"128": 14, "129": 14, "130": 17, "131": 17, "132": 18, "133": 18, "134": 19, "135": 20, "136": 21, "137": 21, "138": 23, "139": 26, "140": 26, "146": 140, "29": 0, "46": 1, "51": 9, "56": 29, "61": 40, "67": 3, "73": 3, "79": 31, "88": 31, "89": 32, "90": 33, "91": 35, "92": 36, "93": 36, "94": 36, "95": 36, "96": 36, "97": 36, "98": 36, "99": 36, "100": 36, "101": 38, "107": 11, "117": 11, "118": 12, "119": 12, "120": 12, "121": 12, "122": 14, "123": 14, "124": 14, "125": 14, "126": 14, "127": 14}, "source_encoding": "utf-8", "uri": "product.html", "filename": "C:/Users/coolp/Google Drive/IS 411 - Systems Design and Implementation/FOMO/catalog/templates/product.html"}
__M_END_METADATA
"""

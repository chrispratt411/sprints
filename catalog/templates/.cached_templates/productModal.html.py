# -*- coding:utf-8 -*-
from mako import runtime, filters, cache
UNDEFINED = runtime.UNDEFINED
STOP_RENDERING = runtime.STOP_RENDERING
__M_dict_builtin = dict
__M_locals_builtin = locals
_magic_number = 10
_modified_time = 1490803942.8039272
_enable_loop = True
_template_filename = 'C:/Users/coolp/Google Drive/IS 411 - Systems Design and Implementation/FOMO/catalog/templates/productModal.html'
_template_uri = 'productModal.html'
_source_encoding = 'utf-8'
import os, os.path, re, json
import django_mako_plus
_exports = ['content']


def _mako_get_namespace(context, name):
    try:
        return context.namespaces[(__name__, name)]
    except KeyError:
        _mako_generate_namespaces(context)
        return context.namespaces[(__name__, name)]
def _mako_generate_namespaces(context):
    pass
def _mako_inherit(template, context):
    _mako_generate_namespaces(context)
    return runtime._inherit_from(context, '/homepage/templates/base_ajax.htm', _template_uri)
def render_body(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        __M_locals = __M_dict_builtin(pageargs=pageargs)
        STATIC_URL = context.get('STATIC_URL', UNDEFINED)
        def content():
            return render_content(context._locals(__M_locals))
        product = context.get('product', UNDEFINED)
        __M_writer = context.writer()
        __M_writer('\r\n')
        if 'parent' not in context._data or not hasattr(context._data['parent'], 'content'):
            context['self'].content(**pageargs)
        

        __M_writer('\r\n')
        return ''
    finally:
        context.caller_stack._pop_frame()


def render_content(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        STATIC_URL = context.get('STATIC_URL', UNDEFINED)
        def content():
            return render_content(context)
        product = context.get('product', UNDEFINED)
        __M_writer = context.writer()
        __M_writer('\r\n<img class="modalpic" src="')
        __M_writer(str( STATIC_URL ))
        __M_writer('homepage/media/Images/')
        __M_writer(str( product.name ))
        __M_writer('.svg" />\r\n<img class="modalpic" src="')
        __M_writer(str( STATIC_URL ))
        __M_writer('homepage/media/Images/banjo.svg" />\r\n<img class="modalpic" src="')
        __M_writer(str( STATIC_URL ))
        __M_writer('homepage/media/Images/clarinet.svg" />\r\n<img class="modalpic" src="')
        __M_writer(str( STATIC_URL ))
        __M_writer('homepage/media/Images/cymbal.svg" />\r\n<img class="modalpic" src="')
        __M_writer(str( STATIC_URL ))
        __M_writer('homepage/media/Images/mic.svg" />\r\n<img class="modalpic" src="')
        __M_writer(str( STATIC_URL ))
        __M_writer('homepage/media/Images/drum-1.svg" />\r\n<img class="modalpic" src="')
        __M_writer(str( STATIC_URL ))
        __M_writer('homepage/media/Images/headphones.svg" />\r\n<img class="modalpic" src="')
        __M_writer(str( STATIC_URL ))
        __M_writer('homepage/media/Images/harp.svg" />\r\n<div style="text-align:center; padding-top:10px;">\r\n  <button id="previous_pic_button" class="btn btn-default btn-lg">Previous</button>\r\n  <button id="next_pic_button" class="btn btn-default btn-lg">Next</button>\r\n</div>\r\n')
        return ''
    finally:
        context.caller_stack._pop_frame()


"""
__M_BEGIN_METADATA
{"source_encoding": "utf-8", "uri": "productModal.html", "filename": "C:/Users/coolp/Google Drive/IS 411 - Systems Design and Implementation/FOMO/catalog/templates/productModal.html", "line_map": {"64": 5, "65": 5, "66": 6, "67": 6, "68": 7, "69": 7, "70": 8, "71": 8, "72": 9, "73": 9, "74": 10, "75": 10, "81": 75, "29": 0, "38": 1, "43": 15, "49": 2, "57": 2, "58": 3, "59": 3, "60": 3, "61": 3, "62": 4, "63": 4}}
__M_END_METADATA
"""

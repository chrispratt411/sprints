# -*- coding:utf-8 -*-
from mako import runtime, filters, cache
UNDEFINED = runtime.UNDEFINED
STOP_RENDERING = runtime.STOP_RENDERING
__M_dict_builtin = dict
__M_locals_builtin = locals
_magic_number = 10
_modified_time = 1491601675.6677358
_enable_loop = True
_template_filename = 'C:/Users/coolp/Google Drive/IS 411 - Systems Design and Implementation/FOMO/catalog/templates/receipt.html'
_template_uri = 'receipt.html'
_source_encoding = 'utf-8'
import os, os.path, re, json
import django_mako_plus
_exports = ['header', 'body_above', 'content']


def _mako_get_namespace(context, name):
    try:
        return context.namespaces[(__name__, name)]
    except KeyError:
        _mako_generate_namespaces(context)
        return context.namespaces[(__name__, name)]
def _mako_generate_namespaces(context):
    pass
def _mako_inherit(template, context):
    _mako_generate_namespaces(context)
    return runtime._inherit_from(context, '/homepage/templates/app_base.htm', _template_uri)
def render_body(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        __M_locals = __M_dict_builtin(pageargs=pageargs)
        cart = context.get('cart', UNDEFINED)
        total = context.get('total', UNDEFINED)
        def body_above():
            return render_body_above(context._locals(__M_locals))
        def content():
            return render_content(context._locals(__M_locals))
        request = context.get('request', UNDEFINED)
        def header():
            return render_header(context._locals(__M_locals))
        __M_writer = context.writer()
        __M_writer('\r\n\r\n')
        if 'parent' not in context._data or not hasattr(context._data['parent'], 'header'):
            context['self'].header(**pageargs)
        

        __M_writer('\r\n\r\n')
        if 'parent' not in context._data or not hasattr(context._data['parent'], 'body_above'):
            context['self'].body_above(**pageargs)
        

        __M_writer('\r\n\r\n')
        if 'parent' not in context._data or not hasattr(context._data['parent'], 'content'):
            context['self'].content(**pageargs)
        

        __M_writer('\r\n')
        return ''
    finally:
        context.caller_stack._pop_frame()


def render_header(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        def header():
            return render_header(context)
        __M_writer = context.writer()
        __M_writer('Receipt')
        return ''
    finally:
        context.caller_stack._pop_frame()


def render_body_above(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        def body_above():
            return render_body_above(context)
        __M_writer = context.writer()
        __M_writer('\r\n  <ol class="breadcrumb">\r\n    <li class="breadcrumb-item"><a href="/homepage/">Homepage</a></li>\r\n    <li class="breadcrumb-item"><a href="/catalog/">Catalog</a></li>\r\n    <li class="breadcrumb-item active">Receipt</a></li>\r\n  </ol>\r\n')
        return ''
    finally:
        context.caller_stack._pop_frame()


def render_content(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        cart = context.get('cart', UNDEFINED)
        total = context.get('total', UNDEFINED)
        request = context.get('request', UNDEFINED)
        def content():
            return render_content(context)
        __M_writer = context.writer()
        __M_writer('\r\n<div id="receipt">\r\n  <p>Thank you for your order of:</p>\r\n')
        for item in cart:
            __M_writer('      <li>')
            __M_writer(str( item.product.name ))
            __M_writer(' </li>\r\n')
        __M_writer('    <p >Your total is $')
        __M_writer(str( total ))
        __M_writer('.</p>\r\n    <p>A confirmation e-mail has been sent to ')
        __M_writer(str( request.user.email ))
        __M_writer('.</p>\r\n</div>\r\n')
        return ''
    finally:
        context.caller_stack._pop_frame()


"""
__M_BEGIN_METADATA
{"source_encoding": "utf-8", "uri": "receipt.html", "line_map": {"64": 3, "97": 13, "98": 16, "99": 17, "100": 17, "101": 17, "70": 3, "103": 19, "104": 19, "105": 20, "106": 20, "43": 1, "76": 5, "112": 106, "48": 3, "82": 5, "53": 11, "88": 13, "58": 22, "102": 19, "29": 0}, "filename": "C:/Users/coolp/Google Drive/IS 411 - Systems Design and Implementation/FOMO/catalog/templates/receipt.html"}
__M_END_METADATA
"""

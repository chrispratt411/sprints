# -*- coding:utf-8 -*-
from mako import runtime, filters, cache
UNDEFINED = runtime.UNDEFINED
STOP_RENDERING = runtime.STOP_RENDERING
__M_dict_builtin = dict
__M_locals_builtin = locals
_magic_number = 10
_modified_time = 1489677199.8575356
_enable_loop = True
_template_filename = 'C:/Users/coolp/Google Drive/IS 411 - Systems Design and Implementation/FOMO/catalog/templates/search.html'
_template_uri = 'search.html'
_source_encoding = 'utf-8'
import os, os.path, re, json
import django_mako_plus
_exports = ['content', 'body_right', 'body_above', 'header', 'body_left']


def _mako_get_namespace(context, name):
    try:
        return context.namespaces[(__name__, name)]
    except KeyError:
        _mako_generate_namespaces(context)
        return context.namespaces[(__name__, name)]
def _mako_generate_namespaces(context):
    pass
def _mako_inherit(template, context):
    _mako_generate_namespaces(context)
    return runtime._inherit_from(context, 'base.htm', _template_uri)
def render_body(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        __M_locals = __M_dict_builtin(pageargs=pageargs)
        def content():
            return render_content(context._locals(__M_locals))
        def body_above():
            return render_body_above(context._locals(__M_locals))
        def body_left():
            return render_body_left(context._locals(__M_locals))
        lastFiveProducts = context.get('lastFiveProducts', UNDEFINED)
        STATIC_URL = context.get('STATIC_URL', UNDEFINED)
        last_five = context.get('last_five', UNDEFINED)
        def body_right():
            return render_body_right(context._locals(__M_locals))
        products = context.get('products', UNDEFINED)
        def header():
            return render_header(context._locals(__M_locals))
        __M_writer = context.writer()
        __M_writer('\r\n\r\n')
        if 'parent' not in context._data or not hasattr(context._data['parent'], 'header'):
            context['self'].header(**pageargs)
        

        __M_writer('\r\n\r\n')
        if 'parent' not in context._data or not hasattr(context._data['parent'], 'body_above'):
            context['self'].body_above(**pageargs)
        

        __M_writer('\r\n\r\n')
        if 'parent' not in context._data or not hasattr(context._data['parent'], 'body_left'):
            context['self'].body_left(**pageargs)
        

        __M_writer('\r\n\r\n')
        if 'parent' not in context._data or not hasattr(context._data['parent'], 'content'):
            context['self'].content(**pageargs)
        

        __M_writer('\r\n\r\n')
        if 'parent' not in context._data or not hasattr(context._data['parent'], 'body_right'):
            context['self'].body_right(**pageargs)
        

        __M_writer('\r\n')
        return ''
    finally:
        context.caller_stack._pop_frame()


def render_content(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        def content():
            return render_content(context)
        products = context.get('products', UNDEFINED)
        STATIC_URL = context.get('STATIC_URL', UNDEFINED)
        __M_writer = context.writer()
        __M_writer('\r\n<div id="products">\r\n')
        for p in products:
            __M_writer('  <div class="aroundProduct">\r\n    <span class="product" id="product')
            __M_writer(str( p.id ))
            __M_writer('">\r\n      <a href="../catalog/product/')
            __M_writer(str( p.id ))
            __M_writer('"><img alt="product - ')
            __M_writer(str( p.name ))
            __M_writer('" src="')
            __M_writer(str( STATIC_URL ))
            __M_writer('homepage/media/Images/')
            __M_writer(str( p.name ))
            __M_writer('.svg" /></a>\r\n    </span>\r\n    <span class="info">\r\n      <h2>')
            __M_writer(str( p.name ))
            __M_writer('</h2>\r\n      <h3>Description:</h3><p>')
            __M_writer(str( p.description ))
            __M_writer('</p>\r\n      <h3>Price:</h3><p>')
            __M_writer(str( p.price ))
            __M_writer('</p>\r\n    </span>\r\n  </div>\r\n')
        __M_writer('</div>\r\n')
        return ''
    finally:
        context.caller_stack._pop_frame()


def render_body_right(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        lastFiveProducts = context.get('lastFiveProducts', UNDEFINED)
        last_five = context.get('last_five', UNDEFINED)
        def body_right():
            return render_body_right(context)
        __M_writer = context.writer()
        __M_writer('\r\n<h1>Recently viewed.</h1>\r\n<ul class="nav">\r\n')
        for p in last_five:
            __M_writer('  <li><a style="color: #df691a; font-size: 30px;" href="../catalog/product/')
            __M_writer(str( p ))
            __M_writer('">')
            __M_writer(str( lastFiveProducts.get(id=p).name ))
            __M_writer('</a></li>\r\n')
        __M_writer('</ul>\r\n')
        return ''
    finally:
        context.caller_stack._pop_frame()


def render_body_above(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        def body_above():
            return render_body_above(context)
        __M_writer = context.writer()
        __M_writer('\r\n  <ol class="breadcrumb">\r\n    <li class="breadcrumb-item"><a href="/homepage/">Homepage</a></li>\r\n    <li class="breadcrumb-item active">Catalog</li>\r\n  </ol>\r\n')
        return ''
    finally:
        context.caller_stack._pop_frame()


def render_header(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        def header():
            return render_header(context)
        __M_writer = context.writer()
        __M_writer('\r\n<span style="padding-left: 300px;padding-right: 50px;">Our Catalog.</span>\r\n<input placeholder="Search products here" style="width: 250px; text-align: center;display:inline-block;" id="search" class="form-control">\r\n</input>\r\n')
        return ''
    finally:
        context.caller_stack._pop_frame()


def render_body_left(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        def body_left():
            return render_body_left(context)
        __M_writer = context.writer()
        __M_writer('\r\n<div id="categories">\r\n  <ul class="nav">\r\n    <h1>Categories.</h1>\r\n    <li><a href="../catalog">All Items</a></li>\r\n    <li><a href="../catalog?category=3">Brass</a></li>\r\n    <li><a href="../catalog?category=2">Children\'s</a></li>\r\n    <li><a href="../catalog?category=7">Keyboard</a></li>\r\n    <li><a href="../catalog?category=5">Miscellaneous</a></li>\r\n    <li><a href="../catalog?category=8">Percussion</a></li>\r\n    <li><a href="../catalog?category=1">Sheet Music</a></li>\r\n    <li><a href="../catalog?category=4">String</a></li>\r\n    <li><a href="../catalog?category=6">Woodwind</a></li>\r\n  </ul>\r\n</div>\r\n')
        return ''
    finally:
        context.caller_stack._pop_frame()


"""
__M_BEGIN_METADATA
{"filename": "C:/Users/coolp/Google Drive/IS 411 - Systems Design and Implementation/FOMO/catalog/templates/search.html", "source_encoding": "utf-8", "line_map": {"133": 9, "139": 9, "145": 3, "151": 3, "29": 0, "163": 16, "169": 163, "157": 16, "48": 1, "53": 7, "58": 14, "63": 31, "68": 48, "73": 57, "79": 33, "87": 33, "88": 35, "89": 36, "90": 37, "91": 37, "92": 38, "93": 38, "94": 38, "95": 38, "96": 38, "97": 38, "98": 38, "99": 38, "100": 41, "101": 41, "102": 42, "103": 42, "104": 43, "105": 43, "106": 47, "112": 50, "120": 50, "121": 53, "122": 54, "123": 54, "124": 54, "125": 54, "126": 54, "127": 56}, "uri": "search.html"}
__M_END_METADATA
"""

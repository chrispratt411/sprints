def addSale(self, totalOfCart):
		stripe_token = self.cleaned_data.get('stripe_token')
		stripe.api_key = settings.STRIPE_SECRET_KEY

		charge = stripe.Charge.create(
		  total=int(totalOfCart),
		  source=token,
          description='Total charge for: ' + self.request.user.username,
		)

		saleRecord = account_models.saleRecord()
		saleRecord.user = self.request.user
        saleRecord.shipping = self.request.user.calc_Shipping()
		saleRecord.address = self.request.session['address']
		saleRecord.city = self.request.session['city']
		saleRecord.state = self.request.session['state']
		saleRecord.zip_code = self.request.session['zip_code']
        saleRecord.stripeID = charge['id']
		saleRecord.subtotal = self.request.user.subtotal()
		saleRecord.tax = self.request.user.tax(self.request.user.subtotal())
        saleRecord.total = self.request.user.subtotal() + self.request.user.tax(self.request.user.subtotal())
		saleRecord.save()

		cart = self.request.user.cart()
		for item in cart:
			saleItem = account_models.SaleItem()
			saleItem.product = item.product
            saleItem.quantity = item.quantity
            saleItem.price = item.product.price
            saleItem.sale = saleRecord
			saleItem.save()
		payment = account_models.Payment()
		payment.sale = saleRecord
		payment.amount = charge['total']
		payment.stripe_ID = charge['id']
		payment.save()
		self.request.session['saleID'] = saleRecord.id
        product = item.product
			if hasattr(item.product, 'quantity'):
				product.quantity = product.quantity - item.quantity
			else:
				product.availability = False
			product.save()

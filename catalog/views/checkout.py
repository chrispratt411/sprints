from django.http import HttpResponseRedirect, HttpResponse
from django_mako_plus import view_function
from django.contrib.auth import authenticate, login, get_user
from django.core.exceptions import ValidationError
from django import forms
from decimal import Decimal
import stripe
from django.conf import settings
from account import models as amod
from .. import dmp_render

@view_function
def process_request(request):
    realHistory = []
    user = request.user
    if user.is_authenticated():
        for h in amod.History.objects.filter(user=user).order_by('-id'):
            if h.product not in realHistory:
                realHistory.append(h.product)
                if len(realHistory) >= 5:
                    break
    else:
        return HttpResponseRedirect('/account/userLogin')

    if request.method == 'POST':
        form = CheckoutForm(request.POST, request, initial={
            'street_address': request.session['street_address'],
        })
        if(form.is_valid()):
            form.commit()
            form.addSale(request)
            saleRecord = amod.SaleRecord.objects.filter(user=user)[0]
            return HttpResponseRedirect('/catalog/receipt/' + str(saleRecord.id))

    else:
        form = CheckoutForm(request.GET, initial={
            'street_address': request.session['street_address'] + ' ' + request.session['city'] + ', ' + request.session['state'] + ' ' + request.session['postal_code'],
        })

    return dmp_render(request, 'checkout.html', {
        'form':form,
        'total':request.user.calc_Total(),
        'subtotal':request.user.calc_subTotal(),
        'tax':request.user.calc_Tax(),
        'shipping':10,
        'realHistory': realHistory,
    })

class CheckoutForm(forms.Form):

    form_submit = 'Pay Now'

    street_address = forms.CharField(label='Shipping Address', required=False, widget=forms.HiddenInput())
    stripe_token = forms.CharField(label='Stripe Token', required=False, widget=forms.HiddenInput())

    def commit(self):
        pass

    def addSale(self, request):
        stripe_token = self.cleaned_data.get('stripe_token')
        stripe.api_key = settings.STRIPE_SECRET_KEY
        amount = int(request.user.calc_Total() * 100)

        charge = stripe.Charge.create(
            amount=amount,
            source=stripe_token,
            currency='usd',
            description='Total charge for: ' + request.user.username,
        )

        saleRecord = amod.SaleRecord()
        saleRecord.full_name = request.session['full_name']
        saleRecord.street_address = request.session['street_address']
        saleRecord.city = request.session['city']
        saleRecord.state = request.session['state']
        saleRecord.postal_code = request.session['postal_code']
        saleRecord.stripeID = charge['id']
        saleRecord.subtotal = request.user.calc_subTotal()
        saleRecord.tax = request.user.calc_Tax()
        saleRecord.total = request.user.calc_Total()
        saleRecord.user = request.user
        saleRecord.save()

        cart = request.user.shoppingCart()
        for item in cart:
            # Create SaleItem
            saleItem = amod.SaleItem()
            saleItem.product = item.product
            saleItem.quantity = item.quantity
            saleItem.price = item.product.price
            saleItem.sale = saleRecord
            saleItem.save()
            # update quantity and availability
            product = item.product
            product.quantity = product.quantity - item.quantity
            if(product.quantity == 0):
                product.available = False
            product.save()
        cart.delete()

        # create SaleItem for tax
        tax = amod.SaleItem()
        tax.sale = saleRecord
        tax.category = 'Fee'
        tax.price = request.user.calc_Tax()
        tax.save()
        # create SaleItem for shipping
        shipping = amod.SaleItem()
        shipping.sale = saleRecord
        shipping.category = 'Fee'
        shipping.price = 10
        shipping.save()

        # create Payment object
        payment = amod.Payment()
        payment.sale = saleRecord
        payment.amount = Decimal(charge['amount'])
        payment.stripe_ID = charge['id']
        payment.save()


# def record_sale(charge_token, user):
    # create Sale Object
    # create SaleItem Objects
    # update Product tables
    # empty shopping cart

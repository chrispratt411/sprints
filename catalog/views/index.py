from django.conf import settings
from django_mako_plus import view_function
from datetime import datetime
from .. import dmp_render, dmp_render_to_string
from catalog import models as catalog_models
from account import models as account_models
from django.http import HttpResponseRedirect, HttpResponse
from django.contrib.auth.decorators import permission_required

@view_function
def process_request(request):
    category = request.GET.get('category')
    products = catalog_models.Product.objects.order_by('-category').filter(available=True)
    user = request.user
    realHistory = []

    if category is not None:
        sortedproducts = products.filter(category=category).all()
    else:
        sortedproducts = products

    if user.is_authenticated():
        for h in account_models.History.objects.filter(user=user).order_by('-id'):
          if h.product not in realHistory:
              realHistory.append(h.product)
              if len(realHistory) >= 5:
                  break

    context = {
        'products': products,
        'realHistory': realHistory,
        'sortedproducts': sortedproducts,
    }
    return dmp_render(request, 'index.html', context)

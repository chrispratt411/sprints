def LastFive(get_response):

    def middleware(request):
        last_5 = request.session.get('last5')
        if last_5 is None:
            last_5 = []
        request.last5 = last_5
        response = get_response(request)
        request.session['last_5'] = request.last5[:6]
        return response
    return middleware

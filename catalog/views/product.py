from django.conf import settings
from django_mako_plus import view_function
from datetime import datetime
from .. import dmp_render, dmp_render_to_string
from catalog import models as catalog_models
from account import models as account_models
from django.core.exceptions import ValidationError
from django.http import HttpResponseRedirect, HttpResponse
from django.contrib.auth.decorators import permission_required, login_required
from django import forms
from formlib.form import FormMixIn

@view_function
def process_request(request):
    # pull all products from the DB
    product = catalog_models.Product.objects.get(id=request.urlparams[0])
    products = catalog_models.Product.objects.order_by('category').all()
    user = request.user
    realHistory = []

    if user.is_authenticated():
        historyProduct = account_models.History()
        historyProduct.user = user
        historyProduct.product = product
        historyProduct.viewDate = datetime.now()
        historyProduct.save()

        for h in account_models.History.objects.filter(user=user).order_by('-id'):
          if h.product not in realHistory:
              realHistory.append(h.product)
              if len(realHistory) >= 5:
                  break

    form = ShoppingCartItemForm(request, product=product)
    if form.is_valid():
        historyProduct.addedToCart = True
        form.commit(product)

    context = {
        'product': product,
        'products': products,
        'form': form,
        'realHistory': realHistory,
    }

    return dmp_render(request, 'product-ajax.html' if request.method == 'POST' else 'product.html', context)

@view_function
def modal(request):
    product = catalog_models.Product.objects.get(id=request.urlparams[0])

    context = {
        'product': product,
    }

    return dmp_render(request, 'productModal.html', context)

class ShoppingCartItemForm(FormMixIn, forms.Form):
    form_submit = 'Add to Cart'
    form_id = 'buy_now_form'

    def init(self, product):
        self.fields['quantity'] = forms.IntegerField(required=False)
        self.product = product

    def commit(self, product):
        request = self.request
        user = request.user
        quantity = self.cleaned_data.get('quantity')

        if product.id in account_models.ShoppingCartItem.objects.values_list('product_id', flat=True):
            shoppingCartItem = account_models.ShoppingCartItem.objects.get(product_id=product.id)
            shoppingCartItem.quantity = shoppingCartItem.quantity + quantity
            shoppingCartItem.save()
        else:
            shoppingCartItem = account_models.ShoppingCartItem()
            shoppingCartItem.user = request.user
            shoppingCartItem.quantity = quantity
            shoppingCartItem.product = catalog_models.Product.objects.get(id=request.urlparams[0])
            shoppingCartItem.save()

        # shoppingCartItem.quantity = quantity
        # product.quantity = product.quantity - int(quantity)
        # product.save()

    def clean_quantity(self):
        formQuantity = self.cleaned_data.get('quantity')
        productQuantity = self.product.quantity

        try:
            cartQuantity = account_models.ShoppingCartItem.objects.get(product_id=self.product.id).quantity
        except account_models.ShoppingCartItem.DoesNotExist:
            cartQuantity = 0

        print("HERE IS MY CARTQUANTITY - ", str(cartQuantity))
        user = self.request.user
        if user.is_authenticated():
            pass
        else:
            raise forms.ValidationError("Must be logged in to buy product")

        if formQuantity is None:
            raise forms.ValidationError("Please select a quantity")

        if productQuantity == 0:
            raise forms.ValidationError("No more left. Sorry!")

        if formQuantity < 0:
            raise forms.ValidationError("We haven't figured out how to sell negative products yet. Sorry!")

        if formQuantity + cartQuantity > productQuantity:
            raise forms.ValidationError("We don't have enough of that product. There are " + str(productQuantity) + " left and you have " + str(cartQuantity) + " in your cart. Sorry!")
        return formQuantity

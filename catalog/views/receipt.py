from django_mako_plus import view_function
from .. import dmp_render
from account import models as amod

@view_function
def process_request(request):
    user = request.user
    realHistory = []

    if user.is_authenticated():
        for h in amod.History.objects.filter(user=user).order_by('-id'):
            if h.product not in realHistory:
                realHistory.append(h.product)
                if len(realHistory) >= 5:
                    break

    try:
        saleRecord = amod.SaleRecord.objects.get(id=request.urlparams[0], user = user)
    except amod.SaleRecord.DoesNotExist:
        return dmp_render(request, 'noReceipt.html', {
            'realHistory':realHistory,
        })
    total = saleRecord.total

    myCart = amod.SaleItem.objects.filter(sale=saleRecord)
    cart = []
    for item in myCart:
        if item.product is not None:
            cart.insert(0, item)


    return dmp_render(request, 'receipt.html', {
        'realHistory': realHistory,
        'saleRecord': saleRecord,
        'cart': cart,
        'total': total,
    })

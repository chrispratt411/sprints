from django.conf import settings
from django_mako_plus import view_function
from datetime import datetime
from .. import dmp_render, dmp_render_to_string
from catalog import models as catalog_models
from django.http import HttpResponseRedirect, HttpResponse
from django.contrib.auth.decorators import permission_required

@view_function
def process_request(request):
    searchString = request.GET["name"]
    products = catalog_models.Product.objects.filter(name__icontains=searchString).all()

    # pull all products from the DB
    lastFiveProducts = catalog_models.Product.objects.all()
    last_five = request.last5

    print('>>>>>> ', products)

    context = {
        'products': products,
        'last_five': last_five,
        'lastFiveProducts': lastFiveProducts,
    }
    return dmp_render(request, 'search.html', context)

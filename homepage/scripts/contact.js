// var contacttype = document.getElementById('id_contactType');
// console.log(contacttype);

// var contacttype2 = $('#id_contactType');
// contacttype2.css('background-color', 'red');
// contacttype2.addClass('asdf');
// console.log(contacttype2);

// console.log('hello world');

// will run when the document is ready //
$(function() {

  // val doesn't work on radio button //
  var contacttype = $('#id_contactType');
  // var value = contacttype.val();
  // console.log(value);

  $('#id_contactType').change(
    function(event) {
      var value = contacttype.val();
      if(value == 'phone'){
        $('.contacttype-phone').closest('p').show();
        $('.contacttype-email').closest('p').hide();
      }else{
        $('.contacttype-phone').closest('p').hide();
        $('.contacttype-email').closest('p').show();
      }
    }
  );

  $('#id_contactType').change();

});

# -*- coding:utf-8 -*-
from mako import runtime, filters, cache
UNDEFINED = runtime.UNDEFINED
STOP_RENDERING = runtime.STOP_RENDERING
__M_dict_builtin = dict
__M_locals_builtin = locals
_magic_number = 10
_modified_time = 1491688243.7665305
_enable_loop = True
_template_filename = 'C:/Users/coolp/Google Drive/IS 411 - Systems Design and Implementation/FOMO/homepage/templates/about.html'
_template_uri = 'about.html'
_source_encoding = 'utf-8'
import os, os.path, re, json
import django_mako_plus
_exports = ['header', 'content']


def _mako_get_namespace(context, name):
    try:
        return context.namespaces[(__name__, name)]
    except KeyError:
        _mako_generate_namespaces(context)
        return context.namespaces[(__name__, name)]
def _mako_generate_namespaces(context):
    pass
def _mako_inherit(template, context):
    _mako_generate_namespaces(context)
    return runtime._inherit_from(context, 'app_base.htm', _template_uri)
def render_body(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        __M_locals = __M_dict_builtin(pageargs=pageargs)
        def header():
            return render_header(context._locals(__M_locals))
        STATIC_URL = context.get('STATIC_URL', UNDEFINED)
        def content():
            return render_content(context._locals(__M_locals))
        __M_writer = context.writer()
        __M_writer('\r\n\r\n')
        if 'parent' not in context._data or not hasattr(context._data['parent'], 'header'):
            context['self'].header(**pageargs)
        

        __M_writer('\r\n')
        if 'parent' not in context._data or not hasattr(context._data['parent'], 'content'):
            context['self'].content(**pageargs)
        

        __M_writer('\r\n')
        return ''
    finally:
        context.caller_stack._pop_frame()


def render_header(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        def header():
            return render_header(context)
        STATIC_URL = context.get('STATIC_URL', UNDEFINED)
        __M_writer = context.writer()
        __M_writer('\r\n<div id="myCarousel" class="carousel slide" data-ride="carousel">\r\n<!-- Indicators -->\r\n<ol class="carousel-indicators">\r\n  <li data-target="#myCarousel" data-slide-to="0" class="active"></li>\r\n  <li data-target="#myCarousel" data-slide-to="1"></li>\r\n  <li data-target="#myCarousel" data-slide-to="2"></li>\r\n  <li data-target="#myCarousel" data-slide-to="3"></li>\r\n</ol>\r\n\r\n<!-- Wrapper for slides -->\r\n<div class="carousel-inner" role="listbox">\r\n  <div class="item active">\r\n    <img src="')
        __M_writer(str( STATIC_URL ))
        __M_writer('homepage/media/Images/carousel-violin.jpg" alt="Piano">\r\n  </div>\r\n\r\n  <div class="item">\r\n    <img src="')
        __M_writer(str( STATIC_URL ))
        __M_writer('homepage/media/Images/carousel-piano.jpg" alt="Violin">\r\n  </div>\r\n\r\n  <div class="item">\r\n    <img src="')
        __M_writer(str( STATIC_URL ))
        __M_writer('homepage/media/Images/carousel-saxophone.jpg" alt="Saxophone">\r\n  </div>\r\n\r\n  <div class="item">\r\n    <img src="')
        __M_writer(str( STATIC_URL ))
        __M_writer('homepage/media/Images/carousel-drums.jpg" alt="Drums">\r\n  </div>\r\n</div>\r\n\r\n<!-- Left and right controls -->\r\n<a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">\r\n  <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>\r\n  <span class="sr-only">Previous</span>\r\n</a>\r\n<a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">\r\n  <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>\r\n  <span class="sr-only">Next</span>\r\n</a>\r\n</div>\r\n')
        return ''
    finally:
        context.caller_stack._pop_frame()


def render_content(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        def content():
            return render_content(context)
        __M_writer = context.writer()
        __M_writer('\r\n  <div id="about">\r\n    <h1>About Us.</h1></br></br>\r\n    <p>We like high-quality instruments. It\'s as simple as that.</p>\r\n    <p style="font-size: 25px;">Accessibility: FOMO has strived to make this site as\r\n      accessible as possible to everyone. Improvements include:</p>\r\n      <ul style="color: #df691a;">\r\n        <li>Alternative image texts</li>\r\n        <li>Large font sizes</li>\r\n      </ul>\r\n\r\n  </div>\r\n  <div id="return">\r\n    <a href="/index/"><i>&lsaquo; Return to home page</i></a>\r\n  </div>\r\n')
        return ''
    finally:
        context.caller_stack._pop_frame()


"""
__M_BEGIN_METADATA
{"uri": "about.html", "filename": "C:/Users/coolp/Google Drive/IS 411 - Systems Design and Implementation/FOMO/homepage/templates/about.html", "source_encoding": "utf-8", "line_map": {"64": 16, "65": 20, "66": 20, "67": 24, "68": 24, "69": 28, "70": 28, "39": 1, "76": 43, "44": 42, "49": 58, "82": 43, "55": 3, "88": 82, "29": 0, "62": 3, "63": 16}}
__M_END_METADATA
"""

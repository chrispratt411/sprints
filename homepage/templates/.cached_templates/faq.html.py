# -*- coding:utf-8 -*-
from mako import runtime, filters, cache
UNDEFINED = runtime.UNDEFINED
STOP_RENDERING = runtime.STOP_RENDERING
__M_dict_builtin = dict
__M_locals_builtin = locals
_magic_number = 10
_modified_time = 1491405446.6001675
_enable_loop = True
_template_filename = 'C:/Users/coolp/Google Drive/IS 411 - Systems Design and Implementation/FOMO/homepage/templates/faq.html'
_template_uri = 'faq.html'
_source_encoding = 'utf-8'
import os, os.path, re, json
import django_mako_plus
_exports = ['content', 'body_above', 'header']


def _mako_get_namespace(context, name):
    try:
        return context.namespaces[(__name__, name)]
    except KeyError:
        _mako_generate_namespaces(context)
        return context.namespaces[(__name__, name)]
def _mako_generate_namespaces(context):
    pass
def _mako_inherit(template, context):
    _mako_generate_namespaces(context)
    return runtime._inherit_from(context, 'app_base.htm', _template_uri)
def render_body(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        __M_locals = __M_dict_builtin(pageargs=pageargs)
        def content():
            return render_content(context._locals(__M_locals))
        def body_above():
            return render_body_above(context._locals(__M_locals))
        def header():
            return render_header(context._locals(__M_locals))
        __M_writer = context.writer()
        __M_writer('\r\n\r\n')
        if 'parent' not in context._data or not hasattr(context._data['parent'], 'header'):
            context['self'].header(**pageargs)
        

        __M_writer('\r\n\r\n')
        if 'parent' not in context._data or not hasattr(context._data['parent'], 'body_above'):
            context['self'].body_above(**pageargs)
        

        __M_writer('\r\n\r\n')
        if 'parent' not in context._data or not hasattr(context._data['parent'], 'content'):
            context['self'].content(**pageargs)
        

        __M_writer('\r\n')
        return ''
    finally:
        context.caller_stack._pop_frame()


def render_content(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        def content():
            return render_content(context)
        __M_writer = context.writer()
        __M_writer('\r\n  <h1 id="question" style="color: #df691a">Is renting an instrument affordable?</h1>\r\n  <p>Absolutely! With FOMO\'s new Play-to-Own pricing option, renting (and buying)\r\n    an instrument has never been easier. For as little as $13 a month, you can\r\n    take an instrument home and play to your heart\'s content! Duis vehicula massa\r\n    interdum auctor viverra. Quisque euismod purus nunc, mattis pharetra lectus\r\n    pellentesque in. Etiam ullamcorper, ligula at congue porttitor, nisi erat\r\n    convallis neque, quis eleifend ipsum eros nec eros. Fusce tristique vitae\r\n    ligula at ornare. Maecenas quis interdum orci. Nam.</p>\r\n  <h1 id="question" style="color: #df691a">Can I learn to play an instrument?</h1>\r\n  <p>Positively! Learning an instrument is as easy as 10 minutes a day. If you\r\n    take 10 minutes every day to instrument practice, you can be a competent player\r\n    in a year. If you have 40 years of life left, you could learn every instrument\r\n    you ever heard of, and then some!\r\n  <h1>\r\n')
        return ''
    finally:
        context.caller_stack._pop_frame()


def render_body_above(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        def body_above():
            return render_body_above(context)
        __M_writer = context.writer()
        __M_writer('\r\n  <ol class="breadcrumb">\r\n    <li class="breadcrumb-item"><a href="/homepage/">Homepage</a></li>\r\n    <li class="breadcrumb-item active">FAQ</li>\r\n  </ol>\r\n')
        return ''
    finally:
        context.caller_stack._pop_frame()


def render_header(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        def header():
            return render_header(context)
        __M_writer = context.writer()
        __M_writer('\r\n  FAQ.\r\n')
        return ''
    finally:
        context.caller_stack._pop_frame()


"""
__M_BEGIN_METADATA
{"uri": "faq.html", "line_map": {"97": 91, "67": 14, "40": 1, "73": 7, "29": 0, "45": 5, "79": 7, "50": 12, "85": 3, "55": 29, "91": 3, "61": 14}, "source_encoding": "utf-8", "filename": "C:/Users/coolp/Google Drive/IS 411 - Systems Design and Implementation/FOMO/homepage/templates/faq.html"}
__M_END_METADATA
"""

# -*- coding:utf-8 -*-
from mako import runtime, filters, cache
UNDEFINED = runtime.UNDEFINED
STOP_RENDERING = runtime.STOP_RENDERING
__M_dict_builtin = dict
__M_locals_builtin = locals
_magic_number = 10
_modified_time = 1488589731.4542408
_enable_loop = True
_template_filename = 'C:/Users/coolp/Google Drive/IS 411 - Systems Design and Implementation/FOMO/homepage/templates/landingpage.html'
_template_uri = 'landingpage.html'
_source_encoding = 'utf-8'
import os, os.path, re, json
import django_mako_plus
_exports = ['header', 'content', 'body_above']


def _mako_get_namespace(context, name):
    try:
        return context.namespaces[(__name__, name)]
    except KeyError:
        _mako_generate_namespaces(context)
        return context.namespaces[(__name__, name)]
def _mako_generate_namespaces(context):
    pass
def _mako_inherit(template, context):
    _mako_generate_namespaces(context)
    return runtime._inherit_from(context, 'app_base.htm', _template_uri)
def render_body(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        __M_locals = __M_dict_builtin(pageargs=pageargs)
        def header():
            return render_header(context._locals(__M_locals))
        def content():
            return render_content(context._locals(__M_locals))
        def body_above():
            return render_body_above(context._locals(__M_locals))
        STATIC_URL = context.get('STATIC_URL', UNDEFINED)
        __M_writer = context.writer()
        __M_writer('\r\n\r\n')
        if 'parent' not in context._data or not hasattr(context._data['parent'], 'header'):
            context['self'].header(**pageargs)
        

        __M_writer('\r\n\r\n')
        if 'parent' not in context._data or not hasattr(context._data['parent'], 'body_above'):
            context['self'].body_above(**pageargs)
        

        __M_writer('\r\n\r\n')
        if 'parent' not in context._data or not hasattr(context._data['parent'], 'content'):
            context['self'].content(**pageargs)
        

        __M_writer('\r\n')
        return ''
    finally:
        context.caller_stack._pop_frame()


def render_header(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        def header():
            return render_header(context)
        __M_writer = context.writer()
        __M_writer('\r\n  Bösendorfer Pianos!!\r\n')
        return ''
    finally:
        context.caller_stack._pop_frame()


def render_content(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        def content():
            return render_content(context)
        STATIC_URL = context.get('STATIC_URL', UNDEFINED)
        __M_writer = context.writer()
        __M_writer('\r\n  <h3>\r\n    FOMO is the best place to get that Bösendorfer piano you\'ve always wanted. With excellent selection, superior quality,\r\n    and unbeatable prices, we think you\'ll agree.\r\n  </h3>\r\n  <br>\r\n  <img style="margin: auto; display: block;" src="')
        __M_writer(str( STATIC_URL ))
        __M_writer('homepage/media/Images/Bosendorfer.png" alt="Bosendorfer" />\r\n  <h4>Buy Bösendorfer pianos <a href="/manager/">here</a>.</h4>\r\n')
        return ''
    finally:
        context.caller_stack._pop_frame()


def render_body_above(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        def body_above():
            return render_body_above(context)
        __M_writer = context.writer()
        __M_writer('\r\n\r\n<ol class="breadcrumb">\r\n  <li class="breadcrumb-item"><a href="/homepage/">Homepage</a></li>\r\n  <li class="breadcrumb-item active">Landing Page</li>\r\n</ol>\r\n\r\n')
        return ''
    finally:
        context.caller_stack._pop_frame()


"""
__M_BEGIN_METADATA
{"source_encoding": "utf-8", "line_map": {"83": 22, "68": 3, "101": 95, "41": 1, "74": 16, "46": 5, "81": 16, "82": 22, "51": 14, "56": 24, "89": 7, "29": 0, "62": 3, "95": 7}, "filename": "C:/Users/coolp/Google Drive/IS 411 - Systems Design and Implementation/FOMO/homepage/templates/landingpage.html", "uri": "landingpage.html"}
__M_END_METADATA
"""

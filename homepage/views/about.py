from datetime import datetime
from django_mako_plus import view_function
from catalog import models as catalog_models
from account import models as account_models
from .. import dmp_render

@view_function
def process_request(request):
    products = catalog_models.Product.objects.order_by('category').all()
    user = request.user
    realHistory = []

    if user.is_authenticated():
        for h in account_models.History.objects.filter(user=user).order_by('-id'):
          if h.product not in realHistory:
              realHistory.append(h.product)
              if len(realHistory) >= 5:
                  break

    context = {
        'now': datetime.now(),
        'products': products,
        'realHistory': realHistory,
    }
    return dmp_render(request, 'about.html', context)

from datetime import datetime
from django_mako_plus import view_function
from catalog import models as catalog_models

@view_function
def process_request(request):
    products = catalog_models.Product.objects.order_by('category').all()

    history = account_models.History.objects.order_by('viewDate')
    myHistory = list(history)
    realHistory = []

    location = "1108 West 1240 N Orem, UT 84057"
    address = geocode(location)
    print(address)

    for p in myHistory:
        if p not in realHistory:
            realHistory.insert(0, p)

    if len(realHistory) > 5:
        realHistory = realHistory[:5]

    context = {
        'now': datetime.now(),
        'last_five': last_five,
        'products': products,
        'realHistory': realHistory,
    }
    return dmp_render(request, 'index.html', context)

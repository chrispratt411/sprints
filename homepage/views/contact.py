from django.http import HttpResponseRedirect, HttpResponse
from django_mako_plus import view_function
from .. import dmp_render
from catalog import models as catalog_models
from account import models as account_models
from django import forms

@view_function
def process_request(request):
    products = catalog_models.Product.objects.order_by('category').all()

    if request.method == 'POST':
        form = ContactForm(request.POST)
        print('>>>>Form is working')
        if(form.is_valid()):
            print('>>>>>', form.cleaned_data)
            email = form.cleaned_data.get('email')
            if form.is_valid:
                return HttpResponseRedirect('/')
    else:
        form = ContactForm()

    user = request.user
    realHistory = []
    if user.is_authenticated():
        for h in account_models.History.objects.filter(user=user).order_by('-id'):
            if h.product not in realHistory:
                realHistory.append(h.product)
                if len(realHistory) >= 5:
                    break

    return dmp_render(request, 'contact.html', {
        'form':form,
        'products':products,
        'realHistory': realHistory,
    })

class ContactForm(forms.Form):
    Subject_Choices = [
        ['payment', 'Payment issue'],
        ['login', "I can't login"],
        ['technical', 'I have a technical issue'],
    ]

    subject = forms.ChoiceField(label='Subject', choices=Subject_Choices,
        widget=forms.Select(attrs={ 'id': 'dropdown' }))
    name = forms.CharField(label='Name', max_length=100,
        widget=forms.TextInput(attrs={ 'class': 'form-control' }))
    contactType = forms.ChoiceField(label='Contact Type', choices=[
        ['phone', 'Phone Number'],
        ['email', 'Email Address'],
    ], initial="phone")
    email = forms.EmailField(label='Email', max_length=100,
        widget=forms.TextInput(attrs={ 'class': 'form-control contacttype-email' }))
    phone = forms.CharField(label='Phone', max_length=100,
        widget=forms.TextInput(attrs={ 'class': 'form-control contacttype-phone' }))
    cell = forms.CharField(label='Cell', max_length=100,
        widget=forms.TextInput(attrs={ 'class': 'form-control contacttype-phone' }))
    message = forms.CharField(label='Message', max_length=1000,
        widget=forms.Textarea(attrs={ 'class': 'form-control' }))

    def clean_name(self):
        name = self.cleaned_data.get('name')
        parts = name.split()
        if len(parts) <=1:
            raise forms.ValidationError('Please give your first and last name.')
        return name

    def clean_email(self):
        email = self.cleaned_data.get('email')
        if not email.lower().strip().endswith('@byu.edu'):
            raise forms.ValidationError('Must have byu.edu email.')
        return email

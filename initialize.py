import os
import django
from django.db import connection
from django.core import management
from datetime import datetime
#
# areyousure = input('''
#   You are about to drop and recreate the entire database.
#   All data are about to be deleted.  Use of this script
#   may cause itching, vertigo, dizziness, tingling in
#   extremities, loss of balance or coordination, slurred
#   speech, temporary zoobie syndrome, longer lines at the
#   testing center, changed passwords in Learning Suite, or
#   uncertainty about whether to call your professor
#   'Brother' or 'Doctor'.
#
#   Please type 'yes' to confirm the data destruction: ''')
# if areyousure.lower() != 'yes':
#     print()
#     print('  Wise choice.')
#     sys.exit(1)

# initialize the django environment
os.environ['DJANGO_SETTINGS_MODULE'] = 'FOMO.settings'
django.setup()


# drop and recreate the database tables
# print()
# print('Living on the edge!  Dropping the current database tables.')
with connection.cursor() as cursor:
    cursor.execute("DROP SCHEMA public CASCADE")
    cursor.execute("CREATE SCHEMA public")
    cursor.execute("GRANT ALL ON SCHEMA public TO postgres")
    cursor.execute("GRANT ALL ON SCHEMA public TO public")

# make the migrations and migrate
management.call_command('makemigrations')
management.call_command('migrate')

from catalog import models as catalog_models
from account import models as account_models
from decimal import Decimal
from django.contrib.auth.models import Permission, Group

uAdd = Permission.objects.get(codename=('add_fomouser'))
uChange = Permission.objects.get(codename=('change_fomouser'))
uDelete = Permission.objects.get(codename=('delete_fomouser'))
pAdd = Permission.objects.get(codename=('add_product'))
pChange = Permission.objects.get(codename=('change_product'))
pDelete = Permission.objects.get(codename=('delete_product'))
bpAdd = Permission.objects.get(codename=('add_bulkproduct'))
bpChange = Permission.objects.get(codename=('change_bulkproduct'))
bpDelete = Permission.objects.get(codename=('delete_bulkproduct'))
upDelete = Permission.objects.get(codename=('add_uniqueproduct'))
upDelete = Permission.objects.get(codename=('change_uniqueproduct'))
upDelete = Permission.objects.get(codename=('delete_product'))


g = Group()
g.name = 'Manager'
g.save()

for p in Permission.objects.all():
    g.permissions.add(p)

g1 = Group()
g1.name = 'Employee'
g1.save()

g1.permissions.add(uAdd)
g1.permissions.add(uChange)
g1.permissions.add(pAdd)
g1.permissions.add(pChange)

chrispratt = account_models.FomoUser()
chrispratt.username = 'chrispratt'
chrispratt.first_name = 'Chris'
chrispratt.last_name = 'Pratt'
chrispratt.email = 'chrispratt@gmail.com'
chrispratt.set_password('cp')
chrispratt.street_address = '1234 Grand Ave'
chrispratt.city = 'Pasimaquadee'
chrispratt.state = 'Ohio'
chrispratt.zip_code = '54432'
chrispratt.phone = '8163242315'
chrispratt.birthdate = datetime(1990, 10, 3)
chrispratt.save()
chrispratt.groups.add(g)
chrispratt.save()

devinbennett = account_models.FomoUser()
devinbennett.username = 'devinbennett'
devinbennett.first_name = 'Devin'
devinbennett.set_password('cp')
devinbennett.last_name = 'Bennett'
devinbennett.email = 'chrispratt@gmail.com'
devinbennett.street_address = '1785 Grand Ave'
devinbennett.city = 'Pasimaquadee'
devinbennett.state = 'Ohio'
devinbennett.zip_code = '54432'
devinbennett.phone = '8012345489'
devinbennett.birthdate = datetime(1992, 2, 7)
devinbennett.save()
devinbennett.groups.add(g1)
devinbennett.save()

brycetrueman = account_models.FomoUser()
brycetrueman.username = 'brycetrueman'
brycetrueman.first_name = 'Bryce'
brycetrueman.set_password('cp')
brycetrueman.last_name = 'Trueman'
brycetrueman.email = 'truemanbryce@gmail.com'
brycetrueman.street_address = '1900 Grand Ave'
brycetrueman.city = 'Pasimaquadee'
brycetrueman.state = 'Ohio'
brycetrueman.zip_code = '54432'
brycetrueman.phone = '8765435435'
brycetrueman.birthdate = datetime(1996, 10, 26)
brycetrueman.save()

stephcurry = account_models.FomoUser()
stephcurry.username = 'stephcurry'
stephcurry.first_name = 'Stephen'
stephcurry.set_password('cp')
stephcurry.last_name = 'Curry'
stephcurry.email = 'stephcurry@gmail.com'
stephcurry.street_address = '1234 Main Street'
stephcurry.city = 'Oakland'
stephcurry.state = 'California'
stephcurry.zip_code = '12345'
stephcurry.phone = '9991234839'
stephcurry.birthdate = datetime(1989, 2, 11)
stephcurry.save()

# users_notfirst = account_models.FomoUser.objects.filter(id__gt=1)
# users_c = account_models.FomoUser.objects.filter(username__startswith="c")
# users_first_alphabet = account_models.FomoUser.objects.order_by('first_name')[0]
# user_oldest = account_models.FomoUser.objects.order_by('birthdate')[0]
# print(users_notfirst, users_c, users_first_alphabet, user_oldest)

sheetmusic = catalog_models.Category()
sheetmusic.code = 'sheet music'
sheetmusic.name = 'Sheet Music'
sheetmusic.save()

children = catalog_models.Category()
children.code = 'children'
children.name = "Children's"
children.save()

brass = catalog_models.Category()
brass.code = 'brass'
brass.name = 'Brass'
brass.save()

string = catalog_models.Category()
string.code = 'string'
string.name = 'String'
string.save()

misc = catalog_models.Category()
misc.code = 'misc'
misc.name = 'Miscellaneous'
misc.save()

woodwind = catalog_models.Category()
woodwind.code = 'woodwind'
woodwind.name = 'Woodwind'
woodwind.save()

keyboard = catalog_models.Category()
keyboard.code = 'keyboard'
keyboard.name = 'Keyboard'
keyboard.save()

percussion = catalog_models.Category()
percussion.code = 'percussion'
percussion.name = 'Percussion'
percussion.save()

tuba = catalog_models.UniqueProduct()
tuba.name = 'Tuba'
tuba.category = brass
tuba.price = Decimal('129.99')
tuba.brand = 'Meinl Weston'
tuba.serial_number = 1001001003
tuba.description = 'A large brass wind instrument of bass pitch, with three to six valves and a broad bell typically facing upward.'
tuba.save()

trumpet = catalog_models.UniqueProduct()
trumpet.name = 'Trumpet'
trumpet.category = brass
trumpet.price = Decimal('109.99')
trumpet.brand = 'LJ Hutcheon'
trumpet.serial_number = 1001001012
trumpet.description = 'A brass musical instrument with a flared bell and a bright, penetrating tone. The modern instrument has the tubing looped to form a straight-sided coil, with three valves.'
trumpet.save()

trombone = catalog_models.UniqueProduct()
trombone.name = 'Trombone'
trombone.category = brass
trombone.price = Decimal('119.99')
trombone.brand = 'Yamaha'
trombone.serial_number = 1001001004
trombone.description = 'A large brass wind instrument with straight tubing in three sections, ending in a bell over the player\'s left shoulder, different fundamental notes being made using a forward-pointing extendable slide.'
trombone.save()

flute = catalog_models.UniqueProduct()
flute.name = 'Flute'
flute.category = woodwind
flute.price = Decimal('179.99')
flute.brand = 'Gemeinhardt'
flute.serial_number = 1001001013
flute.description = 'A wind instrument made from a tube with holes along it that are stopped by the fingers or keys, held vertically or horizontally so that the player\'s breath strikes a narrow edge. The modern orchestral form, typically made of metal, is held horizontally and has an elaborate set of keys.'
flute.save()

clarinet = catalog_models.UniqueProduct()
clarinet.name = 'Clarinet'
clarinet.category = woodwind
clarinet.price = Decimal('199.99')
clarinet.brand = 'Jupiter'
clarinet.serial_number = 1001001014
clarinet.description = 'A woodwind instrument with a single-reed mouthpiece, a cylindrical tube with a flared end, and holes stopped by keys.'
clarinet.save()

oboe = catalog_models.UniqueProduct()
oboe.name = 'Oboe'
oboe.category = woodwind
oboe.price = Decimal('239.99')
oboe.brand = 'Fox'
oboe.serial_number = 1001001015
oboe.description = 'A woodwind instrument with a double-reed mouthpiece, a slender tubular body, and holes stopped by keys.'
oboe.save()

piano = catalog_models.UniqueProduct()
piano.name = 'Piano'
piano.category = keyboard
piano.price = Decimal('8999.99')
piano.brand = 'Bösendorfer'
piano.serial_number = 1001001016
piano.description = 'A large keyboard musical instrument with a wooden case enclosing a soundboard and metal strings, which are struck by hammers when the keys are depressed. The strings\' vibration is stopped by dampers when the keys are released, and it can be regulated for length and volume by two or three pedals.'
piano.save()

piano2 = catalog_models.UniqueProduct()
piano2.name = 'Baldwin Piano'
piano2.category = keyboard
piano2.price = Decimal('8999.99')
piano2.brand = 'Baldwin'
piano2.serial_number = 1001001001
piano2.description = 'A large keyboard musical instrument with a wooden case enclosing a soundboard and metal strings, which are struck by hammers when the keys are depressed. The strings\' vibration is stopped by dampers when the keys are released, and it can be regulated for length and volume by two or three pedals.'
piano2.save()

grandPiano = catalog_models.UniqueProduct()
grandPiano.name = 'Grand Piano'
grandPiano.category = keyboard
grandPiano.price = Decimal('19999.99')
grandPiano.brand = 'Bösendorfer'
grandPiano.serial_number = 1001001005
grandPiano.description = 'A large, full-toned piano that has the body, strings, and soundboard arranged horizontally and in line with the keys and is supported by three legs.'
grandPiano.save()

maracas = catalog_models.BulkProduct()
maracas.name = 'Maracas'
maracas.category = children
maracas.price = Decimal('9.50')
maracas.brand = 'Josef Lidl'
maracas.quantity = 60
maracas.reorder_point = 15
maracas.reorder_amount = 80
maracas.description = 'A small, simple musical instrument consisting of a hollow pipe with a hole in it, over which is a thin covering that vibrates and produces a buzzing sound when the player sings or hums into the pipe.'
maracas.save()

violin = catalog_models.UniqueProduct()
violin.name = 'Violin'
violin.category = string
violin.price = Decimal('129.99')
violin.brand = 'Franz Gustav'
violin.serial_number = 1001001002
violin.description = 'A stringed musical instrument of treble pitch, played with a horsehair bow. The classical European violin was developed in the 16th century. It has four strings and a body of characteristic rounded shape, narrowed at the middle and with two f-shaped sound holes.'
violin.save()

viola = catalog_models.UniqueProduct()
viola.name = 'Viola'
viola.category = string
viola.price = Decimal('129.99')
viola.brand = 'D Z Strad'
viola.serial_number = 1001001007
viola.description = 'An instrument of the violin family, larger than the violin and tuned a fifth lower.'
viola.save()

electric_guitar = catalog_models.UniqueProduct()
electric_guitar.name = 'Electric Guitar'
electric_guitar.category = string
electric_guitar.price = Decimal('229.99')
electric_guitar.brand = 'Ibanez'
electric_guitar.serial_number = 1001001008
electric_guitar.description = 'A guitar with a built-in pickup or pickups that convert string vibrations into electrical signals for amplification.'
electric_guitar.save()

guitar = catalog_models.UniqueProduct()
guitar.name = 'Guitar'
guitar.category = string
guitar.price = Decimal('279.99')
guitar.brand = 'Fender'
guitar.serial_number = 1001001009
guitar.description = 'A stringed musical instrument with a fretted fingerboard, typically incurved sides, and six or twelve strings, played by plucking or strumming with the fingers or a plectrum.'
guitar.save()

ukulele = catalog_models.UniqueProduct()
ukulele.name = 'Ukulele'
ukulele.category = string
ukulele.price = Decimal('89.99')
ukulele.brand = 'Lanikai'
ukulele.serial_number = 1001001017
ukulele.description = 'A small four-stringed guitar of Hawaiian origin.'
ukulele.save()

balalaika = catalog_models.UniqueProduct()
balalaika.name = 'Balalaika'
balalaika.category = string
balalaika.price = Decimal('429.99')
balalaika.brand = 'Hohner'
balalaika.serial_number = 1001001010
balalaika.description = 'A guitarlike musical instrument with a triangular body and two, three, or four strings, popular in Russia and other Slavic countries.'
balalaika.save()

accordion = catalog_models.UniqueProduct()
accordion.name = 'Accordion'
accordion.category = keyboard
accordion.price = Decimal('599.99')
accordion.brand = 'Scandalli'
accordion.serial_number = 1001001011
accordion.description = 'A portable musical instrument with metal reeds blown by bellows, played by means of keys and buttons.'
accordion.save()

sheet = catalog_models.BulkProduct()
sheet.category = sheetmusic
sheet.name = 'Mozart Requiem'
sheet.brand = 'W.E. Hill and Sons'
sheet.price = Decimal('18.50')
sheet.quantity = 10
sheet.reorder_point = 5
sheet.reorder_amount = 10
sheet.description = 'Printed music, as opposed to performed or recorded music.'
sheet.save()

sheet2 = catalog_models.BulkProduct()
sheet2.category = sheetmusic
sheet2.name = 'The Four Seasons'
sheet2.brand = 'W.E. Hill and Sons'
sheet2.price = Decimal('18.50')
sheet2.quantity = 10
sheet2.reorder_point = 5
sheet2.reorder_amount = 10
sheet.description = 'Printed music, as opposed to performed or recorded music.'
sheet2.save()

sheet3 = catalog_models.BulkProduct()
sheet3.category = sheetmusic
sheet3.name = 'Satie Gymnopédie'
sheet3.brand = 'W.E. Hill and Sons'
sheet3.price = Decimal('18.50')
sheet3.quantity = 10
sheet3.reorder_point = 5
sheet3.reorder_amount = 10
sheet.description = 'Printed music, as opposed to performed or recorded music.'
sheet3.save()

whistle = catalog_models.BulkProduct()
whistle.category = children
whistle.name = 'Harmonica'
whistle.brand = 'Jazzo'
whistle.price = Decimal('2.50')
whistle.quantity = 50
whistle.reorder_point = 15
whistle.reorder_amount = 85
whistle.description = 'An aerodynamic whistle is a simple aerophone, an instrument which produces sound from a stream of gas, most commonly air. It may be mouth-operated, or powered by air pressure, steam, or other means.'
# print('>>>>>>>', whistle.name)
whistle.save()

drum_stick = catalog_models.BulkProduct()
drum_stick.category = percussion
drum_stick.name = 'Drum Stick'
drum_stick.brand = 'Wallaby'
drum_stick.price = Decimal('5.50')
drum_stick.quantity = 30
drum_stick.reorder_point = 10
drum_stick.reorder_amount = 40
drum_stick.description = 'A stick, typically with a shaped or padded head, used for beating a drum.'
# print('>>>>>>>', drum_stick.name)
drum_stick.save()

cymbal = catalog_models.BulkProduct()
cymbal.category = percussion
cymbal.name = 'Cymbal'
cymbal.brand = 'Zildjian'
cymbal.price = Decimal('21.50')
cymbal.quantity = 20
cymbal.reorder_point = 10
cymbal.reorder_amount = 30
cymbal.description = 'A musical instrument consisting of a slightly concave round brass plate that is either struck against another one or struck with a stick to make a ringing or clashing sound.'
# print('>>>>>>>', cymbal.name)
cymbal.save()

drum_set = catalog_models.UniqueProduct()
drum_set.category = percussion
drum_set.name = 'Drum Set'
drum_set.brand = 'Wallaby'
drum_set.price = Decimal('899.99')
drum_set.serial_number = 1001001006
drum_set.description = 'A set of drums, cymbals, and other percussion instruments used with drumsticks in jazz and popular music. The most basic components are a foot-operated bass drum, a snare drum, a suspended cymbal, and one or more tom-toms.'
# print('>>>>>>>', drum_set.name)
drum_set.save()

drum = catalog_models.BulkProduct()
drum.category = percussion
drum.name = 'Drum'
drum.brand = 'Pearl'
drum.price = Decimal('21.50')
drum.quantity = 15
drum.reorder_point = 5
drum.reorder_amount = 25
drum.description = 'A percussion instrument sounded by being struck with sticks or the hands, typically cylindrical, barrel-shaped, or bowl-shaped with a taut membrane over one or both ends.'
# print('>>>>>>>', drum.name)
drum.save()

# products_notfirst = catalog_models.Product.objects.filter(id__gt=1)
# products_c = catalog_models.Product.objects.filter(name__startswith="c")
# products_first_alphabet = catalog_models.Product.objects.order_by('name')[0]
# product_most_expensive = catalog_models.Product.objects.order_by('-price')[0]
# for product in products_notfirst:
#     print(product.name)
# for product in products_c:
#     print(product.name)
# print(product_most_expensive.name)
# print(products_first_alphabet.name)

item1 = account_models.History()
item1.user = chrispratt
item1.product = drum
item1.viewDate = datetime.now()
item1.addedToCart = True
item1.purchased = True
item1.save()

cartItem1 = account_models.ShoppingCartItem()
cartItem1.user = chrispratt
cartItem1.product = drum
cartItem1.purchased = True
cartItem1.quantity = 7
cartItem1.save()

item2 = account_models.History()
item2.user = chrispratt
item2.product = drum_stick
item2.viewDate = datetime.now()
item2.addedToCart = True
item2.purchased = True
item2.save()

cartItem2 = account_models.ShoppingCartItem()
cartItem2.user = chrispratt
cartItem2.product = drum_stick
cartItem2.purchased = True
cartItem2.quantity = 14
cartItem2.save()

saleRecord = account_models.SaleRecord()
saleRecord.user = chrispratt
saleRecord.full_name = 'Christopher Pratt'
saleRecord.street_address = '1760 W 300 N'
saleRecord.city = 'Provo'
saleRecord.state = 'UT'
saleRecord.postal_code = '45678'
saleRecord.subtotal = 50.20
saleRecord.stripeID ='345'
saleRecord.tax = 4.15
saleRecord.total = 54.35
saleRecord.save()

saleItem1 = account_models.SaleItem()
saleItem1.product = drum
saleItem1.category = 'Product'
saleItem1.price = 35.50
saleItem1.sale = saleRecord
saleItem1.save()

saleItem1 = account_models.SaleItem()
saleItem1.product = drum_stick
saleItem1.category = 'Product'
saleItem1.price = 14.70
saleItem1.sale = saleRecord
saleItem1.save()

$(function() {

  var producttype = $('#id_product_type');
  // var value = contacttype.val();
  // console.log(value);

  $('#id_product_type').change(
    function(event) {
      console.log('entered function');
      var value = producttype.val();
      console.log(value);
      if(value == 'UniqueProduct'){
        $('#id_serial_number').closest('p').show();
        $('#id_reorder_amount').closest('p').hide();
        $('#id_reorder_point').closest('p').hide();
        $('#id_quantity').closest('p').hide();
      }else if(value == 'BulkProduct'){
        $('#id_serial_number').closest('p').hide();
        $('#id_reorder_amount').closest('p').show();
        $('#id_reorder_point').closest('p').show();
        $('#id_quantity').closest('p').show();
      }
    }
  );

  $('#id_product_type').change();
});

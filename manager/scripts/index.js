$(function() {
  console.log("hey");

  $('.update_button').click( function() {
    console.log('working');
    var pid = $(this).attr('data-pid');
    console.log(pid);

    $(this).siblings(".quantity-container").load('/manager/index.get_quantity/' + pid);

  });

  $('.delete-product').click(function(event) {
    // don't do the normal behavior
    event.preventDefault();

    var link = $(this).attr('href');
    console.log(link);

    $('#delete-button').attr('href', link);

    $('#myModal').modal();
  });

  $('#modallogin_button').click(function() {
    $.loadmodal('/account/userLogin/');
  });

});

# -*- coding:utf-8 -*-
from mako import runtime, filters, cache
UNDEFINED = runtime.UNDEFINED
STOP_RENDERING = runtime.STOP_RENDERING
__M_dict_builtin = dict
__M_locals_builtin = locals
_magic_number = 10
_modified_time = 1491316100.0112095
_enable_loop = True
_template_filename = 'C:/Users/coolp/Google Drive/IS 411 - Systems Design and Implementation/FOMO/homepage/templates/base.htm'
_template_uri = '/homepage/templates/base.htm'
_source_encoding = 'utf-8'
import os, os.path, re, json
import django_mako_plus
_exports = ['body_right', 'header_items', 'maintenance_message', 'header', 'title', 'footer', 'content', 'body_above', 'body_left']


from django_mako_plus import get_template_css, get_template_js 

from datetime import datetime 

def render_body(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        __M_locals = __M_dict_builtin(pageargs=pageargs)
        def header_items():
            return render_header_items(context._locals(__M_locals))
        def content():
            return render_content(context._locals(__M_locals))
        def header():
            return render_header(context._locals(__M_locals))
        STATIC_URL = context.get('STATIC_URL', UNDEFINED)
        def body_above():
            return render_body_above(context._locals(__M_locals))
        def body_left():
            return render_body_left(context._locals(__M_locals))
        def footer():
            return render_footer(context._locals(__M_locals))
        def body_right():
            return render_body_right(context._locals(__M_locals))
        def maintenance_message():
            return render_maintenance_message(context._locals(__M_locals))
        self = context.get('self', UNDEFINED)
        request = context.get('request', UNDEFINED)
        def title():
            return render_title(context._locals(__M_locals))
        user = context.get('user', UNDEFINED)
        __M_writer = context.writer()
        __M_writer('\n')
        __M_writer('\n\n\n<!DOCTYPE html>\n<html>\n    <meta charset="UTF-8">\n    <head>\n\n        <link rel="icon" alt="icon" href="')
        __M_writer(str(STATIC_URL))
        __M_writer('homepage/media/Images/music_note.png" />\n        <title>\n          ')
        if 'parent' not in context._data or not hasattr(context._data['parent'], 'title'):
            context['self'].title(**pageargs)
        

        __M_writer('\n        </title>\n\n')
        __M_writer('        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>\n        <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.5/js/bootstrap.min.js"></script>\n        <script src="')
        __M_writer(str( STATIC_URL ))
        __M_writer('account/scripts/jquery.datetimepicker.full.js"></script>\n        <script src="')
        __M_writer(str( STATIC_URL ))
        __M_writer('homepage/media/jquery.loadmodal.js"></script>\n        <script src="')
        __M_writer(str( STATIC_URL ))
        __M_writer('homepage/media/jquery.form.js"></script>\n\n        <link rel="stylesheet" href="')
        __M_writer(str(STATIC_URL))
        __M_writer('account/media/jquery.datetimepicker.css" />\n        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Josefin+Slab" />\n        <link rel="stylesheet" href="')
        __M_writer(str(STATIC_URL))
        __M_writer('homepage/media/bootstrap/css/bootstrap.css" />\n        <!-- <link href="https://fonts.googleapis.com/css?family=Josefin+Sans" rel="stylesheet"> -->\n')
        if request.dmp_router_page == 'sections':
            __M_writer('        <link rel="stylesheet" href="')
            __M_writer(str(STATIC_URL))
            __M_writer('homepage/media/sections.css" />\n')
        if request.dmp_router_page == 'about':
            __M_writer('        <link rel="stylesheet" href="')
            __M_writer(str(STATIC_URL))
            __M_writer('homepage/media/about.css" />\n')
        if request.dmp_router_page == 'terms':
            __M_writer('        <link rel="stylesheet" href="')
            __M_writer(str(STATIC_URL))
            __M_writer('homepage/media/terms.css" />\n')
        if request.dmp_router_page == 'contact':
            __M_writer('        <link rel="stylesheet" href="')
            __M_writer(str(STATIC_URL))
            __M_writer('homepage/media/contact.css" />\n')
        if request.dmp_router_page == 'faq':
            __M_writer('        <link rel="stylesheet" href="')
            __M_writer(str(STATIC_URL))
            __M_writer('homepage/media/faq.css" />\n')
        __M_writer('        ')
        __M_writer(str( get_template_css(self, request, context) ))
        __M_writer('\n\n    </head>\n\n    <div id="maintenance" style="text-align: center; color: white;">\n      ')
        if 'parent' not in context._data or not hasattr(context._data['parent'], 'maintenance_message'):
            context['self'].maintenance_message(**pageargs)
        

        __M_writer('\n    </div>\n\n    <header>\n      <nav id="my-nav" class="navbar navbar-default">\n        <div class="container-fluid">\n            <div class="navbar-header">\n              <a class="navbar-brand" style="padding-top: 0px; padding-left:10px; padding-right:0px;" href="/homepage/"><img src="')
        __M_writer(str(STATIC_URL))
        __M_writer('homepage/media/Images/music note icon.png" alt="music note" style="height: 67px" /></a>\n            </div>\n          ')
        if 'parent' not in context._data or not hasattr(context._data['parent'], 'header_items'):
            context['self'].header_items(**pageargs)
        

        __M_writer('\n          <ul class="nav navbar-nav navbar-right">\n')
        if user.is_authenticated:
            __M_writer('            <li class="dropdown">\n              <a class="dropdown-toggle" style="margin-right:15px;" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="true">Hello, ')
            __M_writer(str(user.username))
            __M_writer('<span class="caret"></span></a>\n              <ul class="dropdown-menu" style = "margin-right: 15px;">\n                <li><a href="/account/userLogout">Logout</a></li>\n                <li><a href="/account/users/">User Manager</a></li>\n                <li><a href="/manager/">Instrument Manager</a></li>\n                <li><a href="/account/manage/')
            __M_writer(str(user.username))
            __M_writer('">My Account</a></li>\n                <li><a href="/account/changePassword/')
            __M_writer(str(user.username))
            __M_writer('">Change Password</a></li>\n              </ul>\n            </li>\n')
            if user.is_authenticated:
                __M_writer('            <li><a style="padding-top:10px; height: 67px;" class="navbar-brand" href="/account/shoppingCart"><img src="')
                __M_writer(str(STATIC_URL))
                __M_writer('homepage/media/Images/cart.png" alt="cart" style="padding-right: 10px; height: 47px" /><div class="label label-danger" style="color: black; background-color: #5bc0de; height: 18px; padding: 3px; width: 18px; font-size: 14px; position:absolute; top:4px; left:50px;">')
                __M_writer(str( request.user.get_cart_count ))
                __M_writer('</div></a>\n')
        else:
            __M_writer('            <li>\n              <a href="/account/userLogin"><span class="glyphicon glyphicon-log-in"></span> Login</a>\n            </li>\n')
        __M_writer('          </ul>\n        </div>\n      </nav>\n\n')
        if user.is_authenticated:
            __M_writer('      <div class="alert_message alert alert-info">\n        <a href="#" class="close" data-dismiss="alert" aria-label="close" style="color: white;">&times;</a>\n          <strong>You logged on successfully!&nbsp&nbsp</strong>\n      </div>\n')
        elif request.dmp_router_page == 'sections':
            __M_writer('      <div class="alert_message alert alert-info">\n        <a href="#" class="close" data-dismiss="alert" aria-label="close" style="color: white;">&times;</a>\n          <strong>You logged on successfully!&nbsp&nbsp</strong>\n      </div>\n')
        __M_writer('\n    </header>\n    <body>\n      <div class="body_above">\n        <div id="F" style="color: #df691a">F</div>OMO<div id="F" style="color: #df691a">F</div>OMO<div id="F" style="color: #df691a">F</div>OMO<div id="F" style="color: #df691a">F</div>OMO<div id="F" style="color: #df691a">F</div>\n        OMO<div id="F" style="color: #df691a">F</div>OMO<div id="F" style="color: #df691a">F</div>OMO<div id="F" style="color: #df691a">F</div>OMO<div id="F" style="color: #df691a">F</div>OMO<div id="F" style="color: #df691a">F</div>OMO<div id="F" style="color: #df691a">F</div>\n        OMO<div id="F" style="color: #df691a">F</div>OMO<div id="F" style="color: #df691a">F</div>OMO<div id="F" style="color: #df691a">F</div>OMO<div id="F" style="color: #df691a">F</div>OMO<div id="F" style="color: #df691a">F</div>OMO<div id="F" style="color: #df691a">F</div>\n        OMO<div id="F" style="color: #df691a">F</div>OMO<div id="F" style="color: #df691a">F</div>OMO<div id="F" style="color: #df691a">F</div>OMO<div id="F" style="color: #df691a">F</div>OMO<div id="F" style="color: #df691a">F</div>OMO<div id="F" style="color: #df691a">F</div>\n        OMO<div id="F" style="color: #df691a">F</div>OMO<div id="F" style="color: #df691a">F</div>OMO<div id="F" style="color: #df691a">F</div>OMO<div id="F" style="color: #df691a">F</div>OMO<div id="F" style="color: #df691a">F</div>OMO<div id="F" style="color: #df691a">F</div>\n\n        ')
        if 'parent' not in context._data or not hasattr(context._data['parent'], 'body_above'):
            context['self'].body_above(**pageargs)
        

        __M_writer('\n      </div>\n\n      <div class="body_left" alt="violin">\n        ')
        if 'parent' not in context._data or not hasattr(context._data['parent'], 'body_left'):
            context['self'].body_left(**pageargs)
        

        __M_writer('\n      </div>\n\n      <div class="body_middle">\n        <header>\n          <h1 style="color: #5bc0de">')
        if 'parent' not in context._data or not hasattr(context._data['parent'], 'header'):
            context['self'].header(**pageargs)
        

        __M_writer('</h1>\n        </header>\n\n        <div id="content">\n          ')
        if 'parent' not in context._data or not hasattr(context._data['parent'], 'content'):
            context['self'].content(**pageargs)
        

        __M_writer('\n        </div>\n        ')
        if 'parent' not in context._data or not hasattr(context._data['parent'], 'footer'):
            context['self'].footer(**pageargs)
        

        __M_writer('\n      </div>\n\n      <div class="body_right" alt="guitar">\n        ')
        if 'parent' not in context._data or not hasattr(context._data['parent'], 'body_right'):
            context['self'].body_right(**pageargs)
        

        __M_writer('\n      </div>\n\n\n\n')
        __M_writer('      ')
        __M_writer(str( get_template_js(self, request, context) ))
        __M_writer('\n    </body>\n\n    <script type="text/javascript">\n      $(document).ready(function(){\n          $(".alert_message").hide().fadeIn(800).delay(3000).fadeOut(800);\n      });\n    </script>\n\n</html>\n')
        return ''
    finally:
        context.caller_stack._pop_frame()


def render_body_right(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        def body_right():
            return render_body_right(context)
        __M_writer = context.writer()
        __M_writer('\n\n        ')
        return ''
    finally:
        context.caller_stack._pop_frame()


def render_header_items(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        def header_items():
            return render_header_items(context)
        __M_writer = context.writer()
        __M_writer('\n          ')
        return ''
    finally:
        context.caller_stack._pop_frame()


def render_maintenance_message(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        def maintenance_message():
            return render_maintenance_message(context)
        __M_writer = context.writer()
        __M_writer('This site will be down for scheduled maintenance on Saturday, February 11 from 12:00AM - 3:00AM ET.')
        return ''
    finally:
        context.caller_stack._pop_frame()


def render_header(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        def header():
            return render_header(context)
        __M_writer = context.writer()
        return ''
    finally:
        context.caller_stack._pop_frame()


def render_title(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        def title():
            return render_title(context)
        __M_writer = context.writer()
        __M_writer('Homepage - FOMO')
        return ''
    finally:
        context.caller_stack._pop_frame()


def render_footer(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        def footer():
            return render_footer(context)
        __M_writer = context.writer()
        __M_writer('\n          <footer style="padding-top: 15px;">\n            <div class="footer">\n              ')
        __M_writer('\n              ')
        now = datetime.now() 
        
        __M_writer('\n              ')
        year = now.year 
        
        __M_writer('\n              -- &copy ')
        __M_writer(str(year))
        __M_writer(' Copyright -- Family Oriented Music Organization -- All rights reserved --\n            </div>\n          </footer>\n        ')
        return ''
    finally:
        context.caller_stack._pop_frame()


def render_content(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        def content():
            return render_content(context)
        __M_writer = context.writer()
        __M_writer('\n\n          ')
        return ''
    finally:
        context.caller_stack._pop_frame()


def render_body_above(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        def body_above():
            return render_body_above(context)
        __M_writer = context.writer()
        __M_writer('\n\n        <nav class="breadcrumb">\n          <span class="breadcrumb-item active">Homepage</span>\n        </nav>\n\n        ')
        return ''
    finally:
        context.caller_stack._pop_frame()


def render_body_left(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        def body_left():
            return render_body_left(context)
        __M_writer = context.writer()
        return ''
    finally:
        context.caller_stack._pop_frame()


"""
__M_BEGIN_METADATA
{"line_map": {"256": 105, "262": 105, "268": 115, "18": 4, "20": 131, "22": 0, "279": 268, "49": 2, "50": 4, "51": 12, "52": 12, "57": 14, "58": 18, "59": 20, "60": 20, "61": 21, "62": 21, "63": 22, "64": 22, "65": 24, "66": 24, "67": 26, "68": 26, "69": 28, "70": 29, "71": 29, "72": 29, "73": 31, "74": 32, "75": 32, "76": 32, "77": 34, "78": 35, "79": 35, "80": 35, "81": 37, "82": 38, "83": 38, "84": 38, "85": 40, "86": 41, "87": 41, "88": 41, "89": 44, "90": 44, "91": 44, "96": 49, "97": 56, "98": 56, "103": 59, "104": 61, "105": 62, "106": 63, "107": 63, "108": 68, "109": 68, "110": 69, "111": 69, "112": 72, "113": 73, "114": 73, "115": 73, "116": 73, "117": 73, "118": 75, "119": 76, "120": 80, "121": 84, "122": 85, "123": 89, "124": 90, "125": 95, "130": 111, "135": 115, "140": 120, "145": 126, "150": 137, "155": 143, "156": 149, "157": 149, "158": 149, "164": 141, "170": 141, "176": 58, "182": 58, "188": 49, "194": 49, "200": 120, "211": 14, "217": 14, "223": 128, "229": 128, "230": 131, "231": 132, "233": 132, "234": 133, "236": 133, "237": 134, "238": 134, "244": 124, "250": 124}, "source_encoding": "utf-8", "filename": "C:/Users/coolp/Google Drive/IS 411 - Systems Design and Implementation/FOMO/homepage/templates/base.htm", "uri": "/homepage/templates/base.htm"}
__M_END_METADATA
"""

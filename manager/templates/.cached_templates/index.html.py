# -*- coding:utf-8 -*-
from mako import runtime, filters, cache
UNDEFINED = runtime.UNDEFINED
STOP_RENDERING = runtime.STOP_RENDERING
__M_dict_builtin = dict
__M_locals_builtin = locals
_magic_number = 10
_modified_time = 1491316099.853054
_enable_loop = True
_template_filename = 'C:/Users/coolp/Google Drive/IS 411 - Systems Design and Implementation/FOMO/manager/templates/index.html'
_template_uri = 'index.html'
_source_encoding = 'utf-8'
import os, os.path, re, json
import django_mako_plus
_exports = ['content', 'body_above']


def _mako_get_namespace(context, name):
    try:
        return context.namespaces[(__name__, name)]
    except KeyError:
        _mako_generate_namespaces(context)
        return context.namespaces[(__name__, name)]
def _mako_generate_namespaces(context):
    pass
def _mako_inherit(template, context):
    _mako_generate_namespaces(context)
    return runtime._inherit_from(context, '/homepage/templates/app_base.htm', _template_uri)
def render_body(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        __M_locals = __M_dict_builtin(pageargs=pageargs)
        products = context.get('products', UNDEFINED)
        def content():
            return render_content(context._locals(__M_locals))
        STATIC_URL = context.get('STATIC_URL', UNDEFINED)
        def body_above():
            return render_body_above(context._locals(__M_locals))
        hasattr = context.get('hasattr', UNDEFINED)
        __M_writer = context.writer()
        __M_writer('\n<link rel="stylesheet" href="')
        __M_writer(str(STATIC_URL))
        __M_writer('manager/styles/index.css" />\n\n')
        if 'parent' not in context._data or not hasattr(context._data['parent'], 'body_above'):
            context['self'].body_above(**pageargs)
        

        __M_writer('\n\n')
        if 'parent' not in context._data or not hasattr(context._data['parent'], 'content'):
            context['self'].content(**pageargs)
        

        __M_writer('\n')
        return ''
    finally:
        context.caller_stack._pop_frame()


def render_content(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        products = context.get('products', UNDEFINED)
        def content():
            return render_content(context)
        hasattr = context.get('hasattr', UNDEFINED)
        __M_writer = context.writer()
        __M_writer('\n    <h2 style="text-align: center;">Manage Products</h2>\n\n    <table class="table table-striped">\n      <tr>\n        <th>Product Name</th>\n        <th>Quantity</th>\n        <th>Category</th>\n        <th>Price</th>\n        <th>Serial Number</th>\n        <th><a href="/manager/create" class="btn btn-primary" id="create_product">Create Product</a></th>\n      </tr>\n')
        for p in products:
            __M_writer('        <tr>\n          <td>')
            __M_writer(str( p.name ))
            __M_writer('</td>\n          <td>\n')
            if hasattr(p, 'quantity'):
                __M_writer('              <button data-pid="')
                __M_writer(str( p.id ))
                __M_writer('" class="update_button btn btn-info pull-right btn-sm">Update</button>\n              <span class="quantity-container">\n                Click update ->\n              </span>\n')
            else:
                __M_writer('              -\n')
            __M_writer('          </td>\n          <td>')
            __M_writer(str( p.category.name ))
            __M_writer('</td>\n          <td>$')
            __M_writer(str( p.price ))
            __M_writer('</td>\n          <td>\n')
            if hasattr(p, 'serial_number'):
                __M_writer('              ')
                __M_writer(str( p.serial_number ))
                __M_writer('\n')
            else:
                __M_writer('             -\n')
            __M_writer('          </td>\n          <td>\n            <a href="/manager/product/')
            __M_writer(str( p.id ))
            __M_writer('">Edit</a>\n            |\n            <a href="/manager/product.delete/')
            __M_writer(str( p.id ))
            __M_writer('" class="delete-product">Delete</a>\n        </tr>\n')
        __M_writer('    </table>\n\n    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">\n      <div class="modal-dialog" role="document">\n        <div class="modal-content">\n          <div class="modal-header">\n            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>\n            <h4 class="modal-title" id="myModalLabel">Confirm</h4>\n          </div>\n          <div class="modal-body">\n            Are you sure you want to delete the product?\n          </div>\n          <div class="modal-footer">\n            <a id="delete-button" class="btn btn-danger">Yes</a>\n            <button type="button" class="btn btn-default" data-dismiss="modal">No</button>\n          </div>\n        </div>\n      </div>\n    </div>\n')
        return ''
    finally:
        context.caller_stack._pop_frame()


def render_body_above(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        def body_above():
            return render_body_above(context)
        __M_writer = context.writer()
        __M_writer('\r\n  <ol class="breadcrumb">\r\n    <li class="breadcrumb-item"><a href="/homepage/">Homepage</a></li>\r\n    <li class="breadcrumb-item active">Instruments</a></li>\r\n  </ol>\r\n')
        return ''
    finally:
        context.caller_stack._pop_frame()


"""
__M_BEGIN_METADATA
{"line_map": {"67": 11, "68": 23, "69": 24, "70": 25, "71": 25, "72": 27, "73": 28, "74": 28, "75": 28, "76": 32, "77": 33, "78": 35, "79": 36, "80": 36, "81": 37, "82": 37, "83": 39, "84": 40, "85": 40, "86": 40, "87": 41, "88": 42, "89": 44, "90": 46, "91": 46, "92": 48, "29": 0, "94": 51, "112": 106, "100": 4, "41": 1, "42": 2, "43": 2, "93": 48, "48": 9, "53": 70, "59": 11, "106": 4}, "source_encoding": "utf-8", "filename": "C:/Users/coolp/Google Drive/IS 411 - Systems Design and Implementation/FOMO/manager/templates/index.html", "uri": "index.html"}
__M_END_METADATA
"""

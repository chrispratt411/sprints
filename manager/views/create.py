from django.conf import settings
from django_mako_plus import view_function
from datetime import datetime
from .. import dmp_render, dmp_render_to_string
from catalog import models as cmod
from django import forms
from formlib.form import FormMixIn
from django.http import HttpResponseRedirect
from django.core.exceptions import ValidationError
from django.contrib.auth.decorators import permission_required

@view_function
@permission_required('catalog.add_product')
def process_request(request):
    # if method is POST, save data to product
    form = CreateProductForm(request)

    if form.is_valid():
        print('>>>>>form is valid')
        print(form.cleaned_data['name'])
        print(form.cleaned_data['category'])
        product = cmod.Product()
        form.commit(product)
        return HttpResponseRedirect('/manager/')

    context = {
        'form': form,
    }
    return dmp_render(request, 'create.html', context)

product_types = [
    ('UniqueProduct', 'Unique Product'),
    ('BulkProduct', 'Bulk Product'),
    ('RentalProduct', 'Rental Product'),
]

class CreateProductForm(FormMixIn, forms.Form):

    def init(self, product):
        self.fields['product_type'] = forms.ChoiceField(label='Product Type', choices=product_types)
        self.fields['name'] = forms.CharField(label='Product Name', max_length=100)
        self.fields['category'] = forms.ModelChoiceField(label='Category', queryset=cmod.Category.objects.order_by('name').all())
        self.fields['price'] = forms.DecimalField(label='Price')
        self.fields['brand'] = forms.CharField(label='Brand', max_length=100)
        self.fields['quantity'] = forms.IntegerField(label='Quantity', required=False)
        self.fields['reorder_point'] = forms.IntegerField(label='Reorder Point', required=False)
        self.fields['reorder_amount'] = forms.IntegerField(label='Reorder Amount', required=False)
        self.fields['serial_number'] = forms.CharField(label='Serial Number', required=False)

    # def clean_name(self):
    #     name = self.cleaned_data.get('name')
    #     parts = name.split()
    #     if len(parts)

    def commit(self, product):
        product.name = self.cleaned_data.get('name')
        product.category = self.cleaned_data.get('category')
        product.price = self.cleaned_data.get('price')
        product.brand = self.cleaned_data.get('brand')
        if hasattr(product, 'serial_number'):
            product.serial_number = self.cleaned_data.get('serial_number')
        if hasattr(product, 'quantity'):
            product.quantity = self.cleaned_data.get('quantity')
        if hasattr(product, 'reorder_point'):
            product.reorder_point = self.cleaned_data.get('reorder_point')
        if hasattr(product, 'reorder_amount'):
            product.reorder_amount = self.cleaned_data.get('reorder_amount')
        product.save()

from django.conf import settings
from django_mako_plus import view_function
from datetime import datetime
from .. import dmp_render, dmp_render_to_string
from catalog import models as catalog_models
from django.http import HttpResponseRedirect, HttpResponse
from django.contrib.auth.decorators import permission_required

@view_function
@permission_required('catalog.change_product')
def process_request(request):
    # pull all products from the DB
    products = catalog_models.Product.objects.order_by('category').all()
    print('>>>>>> ', products)

    context = {
        'products': products,
    }
    return dmp_render(request, 'index.html', context)

@view_function
def get_quantity(request):
    # Returns current quantity for a given product id
    pid = request.urlparams[0]
    try:
        product = catalog_models.Product.objects.get(id=pid)
    except catalog_models.Product.DoesNotExist:
        return HttpResponseRedirect('/manager/')

    return HttpResponse(product.quantity)

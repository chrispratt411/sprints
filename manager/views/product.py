from django.conf import settings
from django_mako_plus import view_function
from datetime import datetime
from .. import dmp_render, dmp_render_to_string
from catalog import models as cmod
from account import models as amod
from django import forms
from formlib.form import FormMixIn
from django.http import HttpResponseRedirect
from django.core.exceptions import ValidationError
from django.contrib.auth.decorators import permission_required

@view_function
@permission_required('catalog.change_product')
def process_request(request):
    # pull all products from the DB
    try:
        product = cmod.Product.objects.get(id=request.urlparams[0])
    except cmod.Product.DoesNotExist:
        return HttpResponseRedirect('/manager/')

    # if method is POST, save data to product
    form = EditProductForm(request, product=product, initial={
        'name': product.name,
        'category': product.category,
        'price': product.price,
        'brand': product.brand,
        'quantity': getattr(product, 'quantity', 0),
        'serial_number': getattr(product, 'serial_number', 0),
    })

    if form.is_valid():
        print('>>>>>form is valid')
        print(form.cleaned_data['name'])
        print(form.cleaned_data['category'])
        form.commit(product)
        return HttpResponseRedirect('/manager/')

    context = {
        'product': product,
        'form': form,
    }
    return dmp_render(request, 'product.html', context)

class EditProductForm(FormMixIn, forms.Form):

    def init(self, product):
        self.fields['name'] = forms.CharField(label='Product Name', max_length=100)
        self.fields['category'] = forms.ModelChoiceField(label='Category', queryset=cmod.Category.objects.order_by('name').all())
        self.fields['price'] = forms.DecimalField(label='Price')
        self.fields['brand'] = forms.CharField(label='Brand', max_length=100)
        if hasattr(product, 'quantity'):
            self.fields['quantity'] = forms.IntegerField(label='Quantity')
        if hasattr(product, 'serial_number'):
            self.fields['serial_number'] = forms.CharField(label='Serial Number')

    # def clean_name(self):
    #     name = self.cleaned_data.get('name')
    #     parts = name.split()
    #     if len(parts)

    def clean_serial_number(self):
        serial_number = self.cleaned_data.get('serial_number')
        if len(serial_number) == 10 and serial_number.isdigit():
            return serial_number
        else:
            raise forms.ValidationError("Serial number must be 10 digits!")

    def commit(self, product):
        product.name = self.cleaned_data.get('name')
        product.category = self.cleaned_data.get('category')
        product.price = self.cleaned_data.get('price')
        product.brand = self.cleaned_data.get('brand')
        if hasattr(product, 'serial_number'):
            product.serial_number = self.cleaned_data.get('serial_number')
        if hasattr(product, 'quantity'):
            product.quantity = self.cleaned_data.get('quantity')
        product.save()

########################### delete method ###############################
@view_function
@permission_required('catalog.delete_product')
def delete(request):
    try:
        product = cmod.Product.objects.get(id=request.urlparams[0])
    except cmod.Product.DoesNotExist:
        return HttpResponseRedirect('/manager/')

    product.delete()
    return HttpResponseRedirect('/manager/')
